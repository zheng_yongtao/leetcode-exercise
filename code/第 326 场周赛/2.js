/**
 * @param {number[]} nums
 * @return {number}
 */
 var distinctPrimeFactors = function(nums) {
    let pn = new Array(10005).fill(true);
    // pn[1] = false;
    const pArr = [];
    for(let i = 2; i < pn.length; i++){
        if(pn[i]){
            pArr.push(i);
            for(let j = 2; j * i < pn.length; j++){
                pn[i * j] = false;
            }
        }
    }
    let set = new Set();
    for(let i = 0; i < nums.length; i++){
        let temp = nums[i];
        while(!pn[temp]){
            for(let j = 0; j < pArr.length; j++){
                if(temp % pArr[j] == 0){
                    set.add(pArr[j]);
                    temp /= pArr[j];
                }
            }
        }
        if(temp != 1)
        set.add(temp);
    }
    return set.size;
};