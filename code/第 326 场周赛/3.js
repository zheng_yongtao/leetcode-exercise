/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
 var minimumPartition = function(s, k) {
     let res = 0;
     let str = '';
     for(let i = 0; i < s.length; i++){
        if(k < 10 && Number(s[i]) > k) return -1;
         str += s[i];
         if(Number(str) > k){
             str = s[i];
             res++;
         }
     }
     return res+1;
};
let s = "99", k = 9;
// s = "165462", k = 60;
console.log(minimumPartition(s,k));
console.log(Number('9999999999') > 19999999998);