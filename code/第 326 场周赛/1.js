/**
 * @param {number} num
 * @return {number}
 */
 var countDigits = function(num) {
     let n = num;
     let res = 0;
     while(n){
         let val = n % 10;
         if(num % val == 0) res++;
         n = Math.floor(n / 10);
     }
     return res;
};