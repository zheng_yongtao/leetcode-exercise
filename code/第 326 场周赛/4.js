/**
 * @param {number} left
 * @param {number} right
 * @return {number[]}
 */
 var closestPrimes = function(left, right) {
    let pn = new Array(1000005).fill(true);
    pn[1] = false;
    const pArr = [];
    for(let i = 2; i < pn.length; i++){
        if(pn[i]){
            pArr.push(i);
            for(let j = 2; j * i < pn.length; j++){
                pn[i * j] = false;
            }
        }
    }
    let res = Infinity;
    let ans = [-1,-1];
    for(let i = 0; i < pArr.length - 1; i++){
        if(pArr[i] >= left && pArr[i + 1] <= right){
            if(res > (pArr[i + 1] - pArr[i])){
                res = pArr[i + 1] - pArr[i];
                ans = [pArr[i],pArr[i + 1]];
            }
        }
    }
    return ans;
};