/**
 * @param {number[]} chargeTimes
 * @param {number[]} runningCosts
 * @param {number} budget
 * @return {number}
 */
 var maximumRobots = function(chargeTimes, runningCosts, budget) {
    let ct = new MaxPriorityQueue();
    let map = new Map();
    let res = -1,sum = 0;
    let l = 0;
    for(let i = 0; i < chargeTimes.length; i++){
        map.set(chargeTimes[i],(map.get(chargeTimes[i]) || 0) + 1);
        ct.enqueue(chargeTimes[i]);
        sum += runningCosts[i];
        while(!map.get(ct.front().element)) ct.dequeue();
        let cost = ct.front().element + (i - l + 1) * sum;
        while(budget < cost){
            map.set(chargeTimes[l],map.get(chargeTimes[l]) - 1);
            sum -= runningCosts[l++];
            if(sum == 0) break;
            while(!map.get(ct.front().element)) ct.dequeue();
            cost = ct.front().element + (i - l + 1) * sum;
        }
        res = Math.max(res,i - l + 1);
    }
     if(res == -1) res = chargeTimes.length;
    return res;
};