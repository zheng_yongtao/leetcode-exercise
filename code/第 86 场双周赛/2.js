/**
 * @param {number} n
 * @return {boolean}
 */
 var isStrictlyPalindromic = function(n) {
     let check = function(str){
        let l = 0,r = str.length - 1;
        while(l < r){
            if(str[l] != str[r]){
                return false;
            }
            l++;
            r--;
        }
        return true;
     }
     for(let b = 2; b < n - 1; b++){
        let str = n.toString(b);
        if(!check(str)) return false;
     }
     return true;
};