/**
 * @param {number[][]} mat
 * @param {number} cols
 * @return {number}
 */
 var maximumRows = function(mat, cols) {
     let res = 0;
     let list = [];
     let flag = new Array(mat[0].length).fill(false);
     let dfs = function(i,ans = []){
         if(ans.length === cols){
             list.push([...ans]);
             return;
         }
         for(i; i < mat[0].length; i++){
             ans.push(i);
             dfs(i+1,ans);
             ans.pop();
         }
     }
     dfs(0);
     console.log(list);
     for(let i = 0; i < list.length; i++){
         let set = new Set();
         for(let j = 0; j < mat[0].length; j++){
             if(list[i].includes(j)) continue;
             for(let k = 0; k < mat.length; k++){
                 if(mat[k][j] == 1) set.add(k);
             }
         }
         res = Math.max(res,mat.length - set.size);
     }
     return res;
};
let mat = [[0,0,0],[1,0,1],[0,1,1],[0,0,1]], cols = 2;
mat = [[0,1],[1,0]],cols = 2;
console.log(maximumRows(mat, cols));