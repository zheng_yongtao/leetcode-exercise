/**
 * @param {number[]} nums
 * @return {boolean}
 */
 var findSubarrays = function(nums) {
     let map = {};
     for(let i = 1; i < nums.length; i++){
        map[nums[i] + nums[i - 1]] = (map[nums[i] + nums[i - 1]] || 0) + 1;
        if(map[nums[i] + nums[i - 1]] == 2) return true;
     }
     return false;
};
let nums = [1,2,3,4,5];
console.log(findSubarrays(nums));