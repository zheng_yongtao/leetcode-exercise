/**
 * @param {string} s
 * @return {string[]}
 */
var removeInvalidParentheses = function(s) {
    const ans = [];
    let len = 0,l = 0,r = 0;
    for(let i = 0; i < s.length; i++){
        if(s[i] == '(') l++;
        if(s[i] == ')'){
            if(l > r) r++;
            else len++; 
        }
    }
    len += l - r;
    if(len == s.length) return [""];
    const dfs = (s,l,r,i,res)=>{
        if(l < r) return;
        if(i === s.length){
            if(l == r && s.length - len == res.length){
                ans.push(res);
            }
            return;
        }
        for(i;i < s.length; i++){
            if(s[i] == '('){
                dfs(s,l,r,i + 1,res);
                res += '(';
                l++;
            }else if(s[i] == ')'){
                dfs(s,l,r,i + 1,res);
                if(l > r){
                    res += ')';
                    r++;
                }
            }else{
                res += s[i];
            }
        }
        if(l == r && s.length - len == res.length){
            ans.push(res);
        }
    }
    dfs(s,0,0,0,'');
    return [... new Set(ans)];
};