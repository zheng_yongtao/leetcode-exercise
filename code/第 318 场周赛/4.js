/**
 * @param {number[]} robot
 * @param {number[][]} factory
 * @return {number}
 */
 var minimumTotalDistance = function(robot, factory) {
     let res = 0,map = {};
     robot = robot.sort((a,b)=>{return a - b;})
     factory = factory.sort((a,b)=>{return a[0] - b[0];})
     for(const r of robot){
        console.log(factory,res);
        let ind = -1,dist = -1;
        for(let i = 0;i < factory.length; i++){
            if(factory[i][1] == 0) continue;
            const distTemp = Math.abs(factory[i][0] - r);
            if(ind == -1){
                ind = i;
                dist = distTemp;
                continue;
            }
            if(dist <= distTemp) break;
            dist = distTemp;
            ind = i;
        }
        console.log(r,ind,dist);
        factory[ind][1]--;
        res += dist;
     }
     return res;
};
let robot = [0,4,6], factory = [[2,2],[6,2]];
robot = [9,11,99,101],factory = [[10,1],[7,1],[14,1],[100,1],[96,1],[103,1]];
console.log(minimumTotalDistance(robot, factory));