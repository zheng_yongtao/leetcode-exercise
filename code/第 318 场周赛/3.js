/**
 * @param {number[]} costs
 * @param {number} k
 * @param {number} candidates
 * @return {number}
 */
 var totalCost = function(costs, k, candidates) {
    let fontT = new MinPriorityQueue();
    let endT = new MinPriorityQueue();
    let fontInd = candidates - 1;
    let endInd = costs.length - 1 - candidates;
    for(let i = 0; i < candidates; i++){
        fontT.enqueue(costs[i]);
        endT.enqueue(costs[costs.length - 1 - i]);
    }
    let dp1 = new Array(k);
    let dp2 = new Array(k);
    let ind = 0;
    let dp1Ind = [],dp2Ind = [];
    while(ind < k){
        let fontEle = fontT.front()?.element;
        dp1[ind] = fontEle;
        dp1Ind.push(fontInd);
        fontT.dequeue();
        fontInd++;
        if(fontInd < costs.length)
        fontT.enqueue(costs[fontInd]);
        let endEle = endT.front()?.element;
        dp2[ind] = endEle;
        dp2Ind.push(endInd);
        endT.dequeue();
        endInd--;
        if(endInd >= 0)
        endT.enqueue(costs[endInd]);
        ind++;
    }
    console.log(dp1,dp2);
    for(let i = 1 ; i < k; i++){
        dp1[i] += dp1[i - 1];
        dp2[i] += dp2[i - 1];
    }
    let res = Math.min(dp1[k - 1],dp2[k - 1]);
    for(let i = 0; i < k; i++){
        if(dp1Ind >= dp2Ind) break;
        res = Math.min(res,dp1[i] + (dp2[k - 2 - i] || Infinity));
    }
    console.log(dp1,dp2);
    return res;
};