/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
 var maximumSubarraySum = function(nums, k) {
     let map = {};
     let l = 0,sum = 0,res = 0;
     for(let i = 0; i < nums.length; i++){
         if(map[nums[i]] != undefined){
             while(l < map[nums[i]] + 1){
                 sum -= nums[l];
                 l++;
             }
             map[nums[i]] = i;
         }
         sum += nums[i];
         map[nums[i]] = i;
         if((i - l) == (k - 1)){
             console.log(i,l,sum,map);
             res = Math.max(res,sum);
             sum -= nums[l];
             delete map[nums[l]];
             l++;
         }
     }
     return res;
};
let nums = [1,5,4,2,9,9,9], k = 3
console.log(maximumSubarraySum(nums, k));