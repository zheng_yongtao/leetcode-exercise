/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var applyOperations = function(nums) {
    let res = [];
    for(let i = 0; i < nums.length; i++){
        if(i == nums.length - 1){
            res.push(nums[i]);
            break;
        }
        if(nums[i] == nums[i + 1]) {nums[i] *= 2;nums[i + 1] = 0;}
        if(nums[i] != 0) res.push(nums[i]);
    }
    while(res.length < nums.length) res.push(0);
    return res;
};