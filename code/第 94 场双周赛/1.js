/**
 * @param {number[]} forts
 * @return {number}
 */
 var captureForts = function(forts) {
     let res = 0;
     for(let i = 0; i < forts.length; i++){
         if(forts[i] == 1){
             let cnt = 0;
             for(let j = i + 1; j < forts.length; j++){
                 if(forts[j] == 0) cnt++;
                 if(forts[j] == -1) res = Math.max(cnt,res);
                 if(forts[j] != 0) break;
             }
            cnt = 0;
            for(let j = i - 1; j >= 0; j--){
                if(forts[j] == 0) cnt++;
                if(forts[j] == -1) res = Math.max(cnt,res);
                if(forts[j] != 0) break;
            }
         }
     }
     return res;
};