/**
 * @param {number} divisor1
 * @param {number} divisor2
 * @param {number} uniqueCnt1
 * @param {number} uniqueCnt2
 * @return {number}
 */
 var minimizeSet = function(divisor1, divisor2, uniqueCnt1, uniqueCnt2) {
     if(divisor1 > divisor2){
         [divisor1,divisor2,uniqueCnt1,uniqueCnt2] = [divisor2,divisor1,uniqueCnt2,uniqueCnt1];
     }
     console.log('divisor1, divisor2, uniqueCnt1, uniqueCnt2: ', divisor1, divisor2, uniqueCnt1, uniqueCnt2);
     let cnt1 = 0,cnt2 = 0,cnt;
     let i = 1;
     while(i){
         cnt1 = (divisor1 - 1) * i;
         cnt2 = (divisor2 - 1) * i - (cnt1 - Math.floor(((divisor1 - 1) * i) / divisor2));
         console.log(i,cnt1,cnt2);
         if(cnt1 >= uniqueCnt1 && cnt2 + cnt1 - uniqueCnt1 > uniqueCnt2){
             let res1 = divisor2 * i - 1 - (cnt2 + cnt1 - uniqueCnt1 - uniqueCnt2);
             let res2 = divisor1 * i - 1 - cnt1 + uniqueCnt1;
             if(uniqueCnt1 > (divisor1 - 1) * (i - 1) && i > 1) {
                 if(cnt1 + uniqueCnt1 >= divisor1) return res2 - 1;
                 return res2;
             }
             if(res1 % divisor2 == 0) return res1 - 1;
             return res1;
         }
         i++;
     }
};
let divisor1 = 10, divisor2 = 3, uniqueCnt1 = 2, uniqueCnt2 = 10;
divisor1 = 2, divisor2 = 7, uniqueCnt1 = 1, uniqueCnt2 = 3;
divisor1 = 3, divisor2 = 5, uniqueCnt1 = 2, uniqueCnt2 = 1;
divisor1 = 2, divisor2 = 4, uniqueCnt1 = 8, uniqueCnt2 = 2;
divisor1 = 5, divisor2 = 5, uniqueCnt1 = 9, uniqueCnt2 = 3;
divisor1 = 9, divisor2 = 4, uniqueCnt1 = 8, uniqueCnt2 = 3;
// divisor1 = 2557,
// divisor2 = 15901,
// uniqueCnt1 = 805236426,
// uniqueCnt2 = 194763574;
console.log(minimizeSet(divisor1, divisor2, uniqueCnt1, uniqueCnt2));