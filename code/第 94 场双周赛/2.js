/**
 * @param {string[]} positive_feedback
 * @param {string[]} negative_feedback
 * @param {string[]} report
 * @param {number[]} student_id
 * @param {number} k
 * @return {number[]}
 */
 var topStudents = function(positive_feedback, negative_feedback, report, student_id, k) {
    const positiveMap = {},negativeMap = {};
    positive_feedback.forEach(item=>{
        positiveMap[item] = true;
    });
    negative_feedback.forEach(item=>{
        negativeMap[item] = true;
    });
    let res = [];
    for(let i = 0; i < report.length; i++){
        const stu = {id:student_id[i],cnt:0};
        const reps = report[i].split(' ');
        reps.forEach(rep=>{
            if(positiveMap[rep]) stu.cnt += 3;
            if(negativeMap[rep]) stu.cnt -= 1;
        })
        res.push({...stu});
    }
    res = res.sort((a,b)=>{
        if(a.cnt == b.cnt){
            if(a.id.length == b.id.length) return b.id > a.id ? -1 : 1;
            return a.id.length - b.id.length;
        }
        return b.cnt-a.cnt;
    });
    // console.log(res);
    const ans = [];
    res.forEach(item=>{
        ans.push(item.id);
    });
    return ans.slice(0,k);
};
console.log(topStudents(["smart","brilliant","studious"],
["not"],
["this student is studious","the student is smart"],
[1,2],
2));