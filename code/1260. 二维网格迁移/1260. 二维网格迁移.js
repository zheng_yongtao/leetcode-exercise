/**
 * @param {number[][]} grid
 * @param {number} k
 * @return {number[][]}
 */
var shiftGrid = function(grid, k) {
    const arr = [];
    grid.map(item=>{arr.push(...item)});
    for(let i = 0; i < grid.length; i++){
        for(let j = 0; j < grid[i].length; j++){
            const ind = (((i * grid[i].length + j - k) % arr.length) + arr.length) % arr.length;
            grid[i][j] = arr[ind]
        }
    }
    return grid;
};