/**
 * @param {number[]} nums
 * @return {number}
 */
 var findTheArrayConcVal = function(nums) {
    let res = 0;
    while(nums.length){
       res += parseInt(nums.shift() + '' + nums.pop());
    }
    return res;
};