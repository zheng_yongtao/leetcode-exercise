/**
 * @param {string} s
 * @param {number[][]} queries
 * @return {number[][]}
 */
 var substringXorQueries = function(s, queries) {
     let res = [];
     for(const q of queries){
         let t = q[1] ^ q[0];
         t = t.toString(2);
         let ind = s.indexOf(t);
         if(ind == -1){
             res.push([-1,-1]);
         }else{
             res.push([ind,ind + t.length - 1]);
         }
     }
     return res;
};