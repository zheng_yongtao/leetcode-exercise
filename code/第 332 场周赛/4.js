/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
 var minimumScore = function(s, t) {
    let map = {};
    for(let i = 0; i < s.length; i++){
        map[s[i]] = (map[s[i]] || 0) + 1;
    }
    const m = {...map};
    let map1 = {};
    for(let i = 0; i < t.length; i++){
        map1[t[i]] = (map1[t[i]] || 0) + 1;
    }
    const check = (t,map = m,ind = 0) => {
        console.log('t,map: ', t,map);
        for(let i = 0; i < s.length; i++){
            if(ind >= t.length) return true;
            if(!map[t[ind]]) return false;
            console.log('ind',ind);
            if(s[i] == t[ind]) ind++;
            map[s[i]]--;
        }
        console.log('ind >= t.length',ind >= t.length);
        return ind >= t.length;
    }
    let l = -1,r = 0,tmap = {...map};
    for(let i = 0; i < t.length; i++){
        if(!map[t[i]]){
            t[i] = '';
            map1[t[i]]--;
            if(l == -1) l = i;
            r = i;
            map = {...tmap};
            if(check((l == 0 ? '' : t.slice(0,l)) + t.slice(r + 1))) break;
            continue;
        }
        if(map[t[i]] < map1[t[i]]){
            t[i] = '';
            map1[t[i]]--;
            if(l == -1) l = i;
            r = i;
            map = {...tmap};
            if(check((l == 0 ? '' : t.slice(0,l)) + t.slice(r + 1))) break;
            continue;
        }
        console.log('---',i,t[i],map[t[i]],map1[t[i]]);
        tmap = {...map};
        map[t[i]]--;
        map1[t[i]]--;

    }
     console.log(l,r);
     return Math.max(r - l,0) + 1;
};
let s = "abecdebe", t = "eaebceae";
console.log(minimumScore(s,t));