/**
 * @param {number} n
 * @return {number}
 */
 var numberOfCuts = function(n) {
     if(n % 2 == 1) return n;
     return n / 2;
};