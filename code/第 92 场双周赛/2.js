/**
 * @param {number[][]} grid
 * @return {number[][]}
 */
 var onesMinusZeros = function(grid) {
    const row = new Array(grid.length).fill(0),col = new Array(grid[0].length).fill(0);
    for(let i = 0; i < grid.length; i++){
        for(let j = 0; j < grid[i].length; j++){
            row[i] += grid[i][j];
            col[j] += grid[i][j];
        }
    }
    // console.log(row,col);
    const res = new Array(grid.length);
    for(let i = 0; i < grid.length; i++){
        res[i] = new Array(grid[0].length).fill(0);
        for(let j = 0; j < grid[0].length; j++){
            res[i][j] = row[i]  + col[j] - (grid.length - row[i]) - (grid[0].length - col[j]);
        }
    }
    return res;
};