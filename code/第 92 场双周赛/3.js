/**
 * @param {string} customers
 * @return {number}
 */
 var bestClosingTime = function(customers) {
    const N = new Array(customers.length).fill(0);
    const Y = new Array(customers.length).fill(0);
    for(let i = 0; i < customers.length; i++){
        N[i] = (N[i - 1] || 0) + customers[i] == 'N' ? 1 : 0;
        const j = customers.length - i - 1;
        Y[j] = (Y[j + 1] || 0) + customers[j] == 'Y' ? 1 : 0;
    }
    let res = 0,min = Infinity;
    for(let i = 0; i < customers.length; i++){
        if(min > N[i] + Y[i]){
            min = N[i] + Y[i];
            res = i;
        }
    }
    return res;
};