/**
 * @param {string} s
 * @return {string}
 */
var reformat = function(s) {
    const letter = [],number = [];
    for(let i = 0; i < s.length; i++){
        (s[i] >= '0' && s[i] <= '9') ? number.push(s[i]) : letter.push(s[i]);
    }
    if(Math.abs(letter.length - number.length) > 1) return '';
    let res = '';
    while(letter.length){
        res += letter.pop();
        if(!number.length) break;
        res += number.pop();
    }
    if(number.length) res = number.pop() + res; 
    return res;
};