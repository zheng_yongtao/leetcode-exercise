/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var smallestSubarrays = function(nums) {
     let res = nums[nums.length - 1];
     let arr = [...nums];
     for(let i = nums.length - 2;i >= 0; i--){
        res = res | nums[i];
        arr[i] = res;
     }
     let max = arr[0];
     let right = 0;
     let arr1 = [...nums];
     res = nums[0];
     for(let i = 1;i < nums.length; i++){
        res = res | nums[i];
        arr1[i] = res;
        if(res == arr[0] && right == 0) right = i;
     }
     let numCount = new Array(nums.length);
     const len = max.toString(2).length;
     for(let i = 0; i < nums.length; i++){
        let temp = new Array(len).fill(0);
        let t = nums[i].toString(2);
        // console.log('t',t);
        for(let j = 0; j < t.length; j++){
            temp[j] = parseInt(t[j]) + ((i > 0 && parseInt(numCount[i - 1][j])) || 0);
        }
        numCount[i] = temp;
     }
     const cal = function(right,left){
        //  console.log('right,left: ', right,left);
        //  console.log('numCount',numCount);
         let res = '';
         for(let i = 0; i < numCount[right].length; i++){
             res += (numCount[right][i] - numCount[left][i]) > 0 ? '1' : '0';
         }
         console.log('right',right,'left',left);
         console.log('right',numCount[right],'left',numCount[left]);
         console.log('res',res);
         return parseInt(res,2);
     };
     let ans = new Array(nums.length).fill(0);
     for(let i = 0; i < nums.length; i++){
         console.log('iii',max,arr[i],right);
         while(max < arr[i] && right < arr.length - 1){
            right++;
            max = max | nums[right];
         }
         console.log('max',max,right);
         ans[i] = right - i + 1;
        //  max = max ^ nums[i];
        max = cal(right,i);
     }
     console.log(arr,arr1);
     console.log(numCount,max.toString(2));
     return ans;
};