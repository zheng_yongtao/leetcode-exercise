/**
 * @param {number[]} nums
 * @return {number}
 */
 var numberOfPairs = function(nums) {
    let map = new Map();
    const getReverse = function(t){
        let temp = '';
        for(let j = t.length - 1; j >= 0; j--){
            temp += t[j];
        }
        return temp
    };
    for(let i = 0; i < nums.length; i++){
        let temp = getReverse(nums[i] + '');
        temp = temp - nums[i];
        map.set(temp,(map.get(temp) || 0) + 1);
    }
    let res = 0;
    for(let key of map.keys()){
        let ind = map.get(key);
        res += ind * (ind - 1) / 2;
        res %= 10 ** 9 + 7;
    }
    return res;
};