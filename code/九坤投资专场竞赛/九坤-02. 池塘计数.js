/**
 * @param {string[]} field
 * @return {number}
 */
 var lakeCount = function(field) {
    let res = 0;
    for(let i = 0; i < field.length; i++) field[i] = field[i].split('');
    const isOut = (x,y) => {
        return x < 0 || y < 0 || x >= field.length || y >= field[x].length;
    };
    const dfs = (x,y)=>{
        if(isOut(x,y)) return;
        const dx = [0,1,-1,0,-1,-1,1,1];
        const dy = [1,0,0,-1,-1,1,-1,1];
        field[x][y] = 'w';
        for(let i = 0; i < 8; i++){
            let nx = x + dx[i],ny = y + dy[i];
            if(!isOut(nx,ny) && field[nx][ny] == 'W'){
                dfs(nx,ny);
            }
        }
    }
    for(let i = 0; i < field.length; i++){
        for(let j = 0; j < field[i].length; j++){
            if(field[i][j] == 'W'){
                res++;
                dfs(i,j);
            }
        }
    }
    return res;
};

let field = [".....W",".W..W.","....W.",".W....","W.WWW.",".W...."];
console.log(lakeCount(field));