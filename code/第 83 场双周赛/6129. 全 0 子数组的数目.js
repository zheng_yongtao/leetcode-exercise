/**
 * @param {number[]} nums
 * @return {number}
 */
 var zeroFilledSubarray = function(nums) {
    let res = 0;
    let zero = 0;
    for(let i = 0; i < nums.length; i++){
        if(nums[i] == 0) zero++;
        else{
            res += ((1 + zero) * zero) / 2;
            zero = 0;
        }
    }
    if(zero != 0) res += ((1 + zero) * zero) / 2;
    return res;
};