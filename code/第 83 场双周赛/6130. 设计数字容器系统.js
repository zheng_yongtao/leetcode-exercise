
var NumberContainers = function() {
    this.content = new Map();
    this.valueMap = new Map();
    this.MinPriorityQueue = new Map(); 
};

/** 
 * @param {number} index 
 * @param {number} number
 * @return {void}
 */
NumberContainers.prototype.change = function(index, number) {
    let value = this.content.get(index);
    let dList = this.valueMap.get(value);
    // console.log('dList: ', value,dList);
    if(dList) dList.delete(index);
    this.content.set(index,number);
    let list = this.valueMap.get(number) || new Set();
    let q = this.MinPriorityQueue.get(number) || new MinPriorityQueue();
    list.add(index);
    q.enqueue(index);
    this.valueMap.set(number,list);
    this.MinPriorityQueue.set(number,q);
};

/** 
 * @param {number} number
 * @return {number}
 */
NumberContainers.prototype.find = function(number) {
    const list = this.valueMap.get(number);
    const q = this.MinPriorityQueue.get(number);
    if(!list || !list.size) return -1;
    let res = q.front();
    if(!res) return -1;
    res = res.element;
    while(!list.has(res)){
       q.dequeue();
        res = q.front();
        if(!res) return -1;
        res = res.element;
    }
    return res;
};

/**
 * Your NumberContainers object will be instantiated and called as such:
 * var obj = new NumberContainers()
 * obj.change(index,number)
 * var param_2 = obj.find(number)
 */