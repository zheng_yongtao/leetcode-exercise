/**
 * "Flush"：同花，五张相同花色的扑克牌。
    "Three of a Kind"：三条，有 3 张大小相同的扑克牌。
    "Pair"：对子，两张大小一样的扑克牌。
    "High Card"：高牌，五张大小互不相同的扑克牌。
 */
/**
 * @param {number[]} ranks
 * @param {character[]} suits
 * @return {string}
 */
 var bestHand = function(ranks, suits) {
     for(let i = 1; i < 5; i++){
         if(suits[i] != suits[i - 1]) break;
         if(i == 4) return "Flush";
     }
     ranks = ranks.sort();
     for(let i = 1; i < 4; i++){
         if(ranks[i] == ranks[i - 1] && ranks[i] == ranks[i + 1]) return "Three of a Kind";
     }
     for(let i = 1; i < 5; i++){
         if(ranks[i] == ranks[i - 1]) return "Pair";
     }
     return "High Card";
};