/**
 * @param {number[]} vals
 * @param {number[][]} edges
 * @return {number}
 */
 var numberOfGoodPaths = function(vals, edges) {
     let map = {};
     for(let i = 0; i < edges.length; i++){
        let t = map[edges[i][0]] || [];
        t.push(edges[i][1]);
        map[edges[i][0]] = t;
        let t1 = map[edges[i][1]] || [];
        t1.push(edges[i][0]);
        map[edges[i][1]] = t1;
     }
     let flag = {},res = 0,pathSet = new Set();
     const dfs = (node,startNode,visit = new Set(),pre = '',path = '',rePath = '')=>{
         if(flag[path] || flag[rePath] || visit.has(node)) return;
         if(path){
            flag[path] = true;
            flag[rePath] = true;
         }
         if(path && vals[startNode] == vals[node] && !pathSet.has(path) && !pathSet.has(rePath)){
            pathSet.add(path);
            console.log('path: ', path);
            pathSet.add(rePath);
            res++;
         }
         let list = map[node] || [];
         for(let i = 0; i < list.length; i++){
             if((!pre || vals[list[i]] <= vals[pre])){
                visit.add(node);
                let tp = path ? (path + '-' +  node) : node;
                let rp = rePath ? (node + '-' + rePath) : node;
                 dfs(list[i],startNode,visit,node,tp,rp);
             }
         }
     };
     for(let i = 0; i < vals.length; i++){
        dfs(i,i);
     }
     console.log(flag);
    //  console.log(map);
     return res;
};

let vals = [1,3,2,1,3], edges = [[0,1],[0,2],[2,3],[2,4]];
vals = [2,5,5,1,5,2,3,5,1,5],edges = [[0,1],[2,1],[3,2],[3,4],[3,5],[5,6],[1,7],[8,4],[9,7]];
console.log(numberOfGoodPaths(vals, edges));
