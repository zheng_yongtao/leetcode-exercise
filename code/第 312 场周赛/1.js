/**
 * @param {string[]} names
 * @param {number[]} heights
 * @return {string[]}
 */
var sortPeople = function(names, heights) {
    let arr = [];
    for(let i = 0; i < names.length; i++){
        let obj = {};
        obj.names = names[i];
        obj.heights = heights[i];
        arr.push(obj);
    }
    arr  = arr.sort((a,b)=>{
        return b.heights - a.heights;
    });
    let res = [];
    for(let i = 0; i < arr.length; i++){
        res.push(arr[i].names);
    }
    return res;
};