/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
 var goodIndices = function(nums, k) {
    let arr1 = new Array(nums.length).fill(1);
    let arr2 = new Array(nums.length).fill(1);
    const len = nums.length;
    for(let i = 1; i < len; i++){
        if(nums[i] <= nums[i - 1]){
            arr1[i] = arr1[i - 1] + 1;
        }
        if(nums[len - 1 - i] <= nums[len - i]){
            arr2[len - 1 - i] = arr2[len - i] + 1;
        }
        // if(nums[i] >= nums[i - 1]){
        //     arr2[i] = arr2[i - 1] + 1
        // }
    }
    console.log(arr1,arr2);
    let res = [];
    for(let i = k; i < nums.length - k; i++){
        // if(k == 1) res.push(i);
        if(arr1[i - 1] >= k && arr2[i + 1] >= k) res.push(i);
    }
    return res;
};
let nums = [478184,863008,716977,921182,182844,350527,541165,881224],k = 1;
console.log(goodIndices(nums,k));