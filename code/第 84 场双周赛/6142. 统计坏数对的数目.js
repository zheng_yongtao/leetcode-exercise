/**
 * @param {number[]} nums
 * @return {number}
 */
 var countBadPairs = function(nums) {
    let flag = {};
    for(let i = 0; i < nums.length; i++){
        flag[nums[i] - i] = (flag[nums[i] - i] || 0) + 1;
    }
    let res = 0;
    let len = nums.length;
    for(let key in flag){
        res += (len - flag[key]) * flag[key];
    }
    return res / 2;
};