/**
 * @param {number[]} nums
 * @return {number}
 */
 var minimumReplacement = function(nums) {
    let res = 0,min = nums.pop();
    while (nums.length){
        let k = nums.pop();
        let tmp = Math.floor((k - 1) / min) + 1;
        res += tmp - 1;
        min = Math.floor(k / tmp);
    }
    return res
};