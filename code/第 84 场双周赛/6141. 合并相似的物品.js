/**
 * @param {number[][]} items1
 * @param {number[][]} items2
 * @return {number[][]}
 */
 var mergeSimilarItems = function(items1, items2) {
     let o1 = {},o2 = {},key = new Set();
     items1.map(item=>{
         o1[item[0]] = item[1];
         key.add(item[0]);
     });
     items2.map(item=>{
         o2[item[0]] = item[1];
         key.add(item[0]);
     });
     key = [...key].sort((a,b)=>{
         return a - b;
     });
     let res = [];
     for(let i = 0; i < key.length; i++){
         res.push([key[i],(o1[key[i]] || 0) + (o2[key[i]] || 0)]);
     }
     return res;
};