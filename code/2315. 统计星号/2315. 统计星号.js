/**
 * @param {string} s
 * @return {number}
 */
 var countAsterisks = function(s) {
    let res = 0;
    s = s.split('|');
    for(let i = 0; i < s.length; i += 2){
        for(let j = 0; j < s[i].length; j++){
            if(s[i][j] === '*') res++;
        }
    }
    return res;
};