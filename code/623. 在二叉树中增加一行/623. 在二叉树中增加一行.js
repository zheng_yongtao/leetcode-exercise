/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} val
 * @param {number} depth
 * @return {TreeNode}
 */
 var addOneRow = function(root, val, depth) {
    if(depth == 1) return new TreeNode(val,root);
    const dfs = function(r,f){
        if(!r) return;
        if(f == depth - 1){
            r.left = new TreeNode(val,r.left);
            r.right = new TreeNode(val,null,r.right);
            return;
        }
        dfs(r.left, f + 1);
        dfs(r.right,f + 1);
    }
    dfs(root,1);
    return root;
};