/**
 * @param {number[]} edges
 * @param {number} node1
 * @param {number} node2
 * @return {number}
 */
 var closestMeetingNode = function(edges, node1, node2) {
    let flag = new Array(edges.length).fill(Infinity);
    flag[node1] = 0;
    const dfs = (node,map,step = 0) => {
        if(map[node] != undefined && map[node] <= step) return;
        map[node] = step;
        if(edges[node] == -1) return;
        dfs(edges[node],map,step + 1);
    }
    let map1 = {};
    dfs(node1,map1);
    let map2 = {};
    dfs(node2,map2);
    let res = Infinity;
    let ans = Infinity;
    for(let k in map1){
        if(map2[k] == undefined) continue;
        if(Math.max(map1[k] , map2[k]) <= res){
           if(Math.max(map1[k] , map2[k]) == res && k > ans) continue;
           res = Math.max(map1[k] , map2[k]);
           ans = k;
       }
    }
    return ans == Infinity ? -1 : ans;
};