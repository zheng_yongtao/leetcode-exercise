/**
 * @param {number[]} nums
 * @return {number}
 */
 var minimumOperations = function(nums) {
    const res = new Set();
    nums.map(item=>{
        item != 0 && res.add(item);
    });
    return res.size;
};