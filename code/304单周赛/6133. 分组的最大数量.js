/**
 * @param {number[]} grades
 * @return {number}
 */
 var maximumGroups = function(grades) {
    let res = 1;
    while(res++){
        if(((1 + res) * res) / 2 > grades.length) break;
    }
    return res - 1;
};