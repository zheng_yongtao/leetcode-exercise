/**
 * @param {number[]} edges
 * @return {number}
 */
 var longestCycle = function(edges) {
    let res = -1;
    const dfs = (node,steps = {},step = 0)=>{
        if(visited[node] || steps[node] <= step || edges[node] == -1){
            if(steps[node] < step) res = Math.max(step - steps[node],res);
            return;
        }
        steps[node] = step; 
        visited[node] = true;
        dfs(edges[node],steps,step + 1);
    }
    const visited = new Array(edges.length).fill(false);
    for(let i = 0; i < edges.length; i++) dfs(i);
    return res;
};