var MagicDictionary = function() {
    this.tree = {};
};

/** 
 * @param {string[]} dictionary
 * @return {void}
 */
MagicDictionary.prototype.buildDict = function(dictionary) {
    this.dictionary = dictionary;
    this.buildTree(dictionary);
};

MagicDictionary.prototype.buildTree = function(dictionary) {
    dictionary.map(item=>{
        let tree = this.tree;
        for(const ch of item){
            if(!tree[ch]) tree[ch] = {};
            tree = tree[ch];
        }
        tree.isEnd = true;
    })
};

MagicDictionary.prototype.judge = function(searchWord,i,canReplace = true,tree = this.tree) {
    const ch = searchWord[i];
    if(canReplace){
        for(const k in tree){
            if(i == searchWord.length - 1){
                if(tree[k].isEnd && k != ch) return true;
            }else{
                if(this.judge(searchWord,i+1,k == ch,tree[k])) return true;
            }
        }
        return false;
    }else{
        if(tree[ch]){
            if(i == searchWord.length - 1) return tree[ch].isEnd == true;
            return this.judge(searchWord,i+1,canReplace,tree[ch]);
        }
        return false;
    }
};
/** 
 * @param {string} searchWord
 * @return {boolean}
 */
MagicDictionary.prototype.search = function(searchWord) {
    return this.judge(searchWord,0);
};

/**
 * Your MagicDictionary object will be instantiated and called as such:
 * var obj = new MagicDictionary()
 * obj.buildDict(dictionary)
 * var param_2 = obj.search(searchWord)
 */