/**
 * @param {number[][]} grid
 * @param {number} k
 * @return {number}
 */
 var numberOfPaths = function(grid, k) {
    const mod = 10 ** 9 + 7;
    let dp = [];
    for (let i = 0; i < grid.length; i++) {
        dp[i] = new Array(grid[i].length);
        for (let j = 0; j < dp[i].length; j++) {
            dp[i][j] = new Array(k).fill(0);
        }
    }
    dp[0][0][grid[0][0] % k] = 1;

    for (let i = 0; i < grid.length; i++) {
        for (let j = 0; j < grid[i].length; j++) {
            let now = grid[i][j];
            if (i > 0) {
                for (let num = 0; num < k; num++) {
                    dp[i][j][(num + now) % k] += dp[i - 1][j][num];
                    dp[i][j][(num + now) % k] %= mod;
                }
            }
            if (j > 0) {
                for (let num = 0; num < k; num++) {
                    dp[i][j][(num + now) % k] += dp[i][j - 1][num];
                    dp[i][j][(num + now) % k] %= mod;
                }
            }
        }
    }
    return dp[grid.length - 1][grid[0].length - 1][0]
};