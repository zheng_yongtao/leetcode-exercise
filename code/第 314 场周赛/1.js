/**
 * @param {string} time
 * @return {number}
 */
 var countTime = function(time) {
    let a = 1,b = 1;
    if(time[0] == '?' && time[1] == '?') a = 24;
    else if(time[0] == '?') a = time[1] > 3 ? 2 : 3;
    else if(time[1] == '?') a = time[0] == '2' ? 4 : 10;
    
    if(time[3] == '?') b = 6;
    if(time[4] == '?') b = time[3] == '?' ? 60 : 10;
    return a * b;
};