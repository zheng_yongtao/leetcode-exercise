/**
 * @param {number[][]} intervals
 * @return {number[]}
 */
 var findRightInterval = function (intervals) {
  const map = new Map();
  const res = new Array(intervals.length).fill(-1);
  for(let i = 0; i < intervals.length; i++){
    map.set(intervals[i][0] + '-' + intervals[i][1],i);
  }
  intervals = intervals.sort((a,b)=>{
    if(a[0] === b[0]) return a[1] - b[1];
    return a[0] - b[0];
  });
  const findRight = (i,l,r)=>{
    while(l < r){
      const mid = Math.floor((l + r) / 2);
      if(intervals[mid][0] < intervals[i][1]) l = mid + 1;
      else r = mid;
    }
    if(l == intervals.length) return -1;
    return map.get(intervals[l][0] + '-' + intervals[l][1]);
  };
  for(let i = 0; i < intervals.length; i++){
    const ind = map.get(intervals[i][0] + '-' + intervals[i][1]);
    res[ind] = findRight(i,i,intervals.length);
  }
  return res;
};

let intervals = [[3,4],[2,3],[1,2]];
intervals = [[1,4],[2,3],[3,4]];
intervals = [[1,2],[2,3],[0,1],[3,4]];
console.log(findRightInterval(intervals));