/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
 var maxKelements = function(nums, k) {
    let m = new MaxPriorityQueue();
    for(const n of nums) m.enqueue(n);
    let res = 0;
    while(k--){
        let score = m.front().element;
        m.dequeue();
        m.enqueue(Math.ceil(score / 3));
        res += score;
    }
    return res;
};