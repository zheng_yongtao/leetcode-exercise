/**
 * @param {string} word1
 * @param {string} word2
 * @return {boolean}
 */
 var isItPossible = function(word1, word2) {
     let m1 = {},m2 = {};
     for(let i = 0; i < word1.length; i++) m1[word1[i]] = (m1[word1[i]] || 0) + 1;
     for(let i = 0; i < word2.length; i++) m2[word2[i]] = (m2[word2[i]] || 0) + 1;
     let k1 = Object.keys(m1).length,k2 = Object.keys(m2).length;
     console.log(k1,k2,m1,m2);
     for(const key1 in m1){
         for(const key2 in m2){
             let kk1 = k1,kk2 = k2;
             if(key1 == key2){
                 if(kk1 == kk2) return true;
                 continue;
             }
             if(m1[key1] == 1) kk1--;
             if(!m2[key1]) kk2++;
             if(!m1[key2]) kk1++;
             if(m2[key2] == 1) kk2--;
             if(kk1 == kk2) return true;
         }
     }
     return false;
};
let word1 = "abcc", word2 = "aab";
word1 = "abcde", word2 = "fghij";
word1 = "aa", word2 = "bb";
word1 = "ab", word2 = "abcc";
word1 = "aa", word2 = "ab";
// word1 = "aab", word2 = "bccd";
// word1 = "wilfuzpxqserkdcvbgajtyhon",word2 = "rlmyvwvucqxsjodbelmgjkabnxegihuwats";
// word1 = "buebek",word2 = "ukuk";
console.log(isItPossible(word1, word2));