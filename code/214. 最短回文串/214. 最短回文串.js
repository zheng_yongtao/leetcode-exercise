/**
 * @param {string} s
 * @return {string}
 */
var shortestPalindrome = function(s) {
    const judge = function(str){
        let l = 0,r = str.length - 1;
        while(l <= r){
            if(str[l++] != str[r--]) return false;
        }
        return true;
    }
    if(judge(s)) return s;
    let temp = '';
    for(let i = s.length - 1; i >= 0; i--){
        temp += s[i];
        if(judge(temp + s)) return temp + s;
    }
    return '';
};