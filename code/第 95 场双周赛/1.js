/**
 * @param {number} length
 * @param {number} width
 * @param {number} height
 * @param {number} mass
 * @return {string}
 */
 var categorizeBox = function(length, width, height, mass) {
     let Bulky = false,Heavy = false;
     if(length >= 10000 || width >= 10000 || height >= 10000) Bulky = true;
     if(length * width * height >= 10 ** 9) Bulky = true;
     if(mass >= 100) Heavy = true;
     if(Bulky && Heavy) return 'Both';
     if(Bulky) return 'Bulky';
     if(Heavy) return 'Heavy';
     return 'Neither';
};