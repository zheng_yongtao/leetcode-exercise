/**
 * @param {number} value
 * @param {number} k
 */
 var DataStream = function(value, k) {
     this.arr = new Array(k).fill(-1);
     this.value = value;
     this.k = k;
     this.ind = 0;
     this.isNotVal = 0;
};

/** 
 * @param {number} num
 * @return {boolean}
 */
DataStream.prototype.consec = function(num) {
    if(this.arr[this.ind] != -1 && this.arr[this.ind] != this.value) this.isNotVal--;
    this.arr[this.ind] = num;
    this.ind++;
    this.ind %= this.k;
    if(num != this.value) this.isNotVal++;
    return this.arr[this.k -1] == this.value && this.isNotVal == 0;
};

/**
 * Your DataStream object will be instantiated and called as such:
 * var obj = new DataStream(value, k)
 * var param_1 = obj.consec(num)
 */