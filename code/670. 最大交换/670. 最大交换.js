/**
 * @param {number} num
 * @return {number}
 */
var maximumSwap = function(num) {
    num += '';
    let sNum = num.split('').sort((a,b)=>b-a);
    let res = num.split('');
    for(let i = 0; i < res.length; i++){
        if(res[i] != sNum[i]){
            for(let j = res.length - 1; j > i; j--){
                if(res[j] == sNum[i]){
                    [res[i],res[j]] = [res[j],res[i]];
                    break;
                }
            }
            break;
        }
    }
    return res.join('');
};

let num = '2736';
num = '9973';
console.log(maximumSwap(num));