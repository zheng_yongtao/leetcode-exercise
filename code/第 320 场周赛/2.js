/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number[]} queries
 * @return {number[][]}
 */
 var closestNodes = function(root, queries) {
    let arr = [];
    const dfs = (r)=>{
        if(!r) return;
        arr.push(r.val);
        dfs(r.left);
        dfs(r.right);
    }
    dfs(root);
    arr = arr.sort((a,b)=>{
        return a - b;
    });
    console.log(arr);
    let res = [];
    const querie = (num)=>{
        let l = 0,r = arr.length - 1;
        while(l < r){
           let m = (l + r) >> 1;
           if(arr[m] == num) return m;
           if(arr[m] > num) r = m;
           else l = m + 1;
        }
        return l;
    };
    for(let i = 0; i < queries.length; i++){
        let n = querie(queries[i]);
        console.log(queries,n);
        let r = [];
        if(arr[n] == queries[i]) r = [arr[n],arr[n]];
        else{
            if(arr[n] > queries[i]){
                r = [arr[n - 1] || (- 1),arr[n] || (- 1)];
            }else{
                r = [arr[n] || (-1),arr[n + 1] || (- 1)];
            }
        }
        res.push(r);
    }
    return res;
};