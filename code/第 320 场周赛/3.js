/**
 * @param {number[][]} roads
 * @param {number} seats
 * @return {number}
 */
 var minimumFuelCost = function(roads, seats) {
    // console.log('----');
    if(roads.length == 0) return 0;
   let sp = new Array(roads.length + 1).fill(0);
   let map = {};
   let map1 = {};
   let spNode = [];
   for(let i = 0; i < roads.length; i++){
        sp[roads[i][0]] ++;
        sp[roads[i][1]] ++;
        if(map[roads[i][0]] == undefined)
            map[roads[i][0]] = roads[i][1];
        if(map1[roads[i][1]] == undefined)
            map1[roads[i][1]] = roads[i][0];
   }
    // console.log(sp);
    for(let i = 0; i < sp.length; i++){
        if(sp[i] == 1) spNode.push(i);
    }
   let res = 0;
   console.log('spNode',spNode);
   for(const i of spNode){
        let cnt = 1;
        const visited = new Array(roads.length + 2).fill(false);
        visited[i] = true;
        let next = map[i] || map1[i];
        while(next != 0){
        //   console.log('--',i,next,visited);
            cnt++;
            visited[next] = true;
            if(map[next] != undefined && !visited[map[next]]){
                next = map[next];
            }else{
                next = map1[next];
            }
        }
    //   console.log(i,cnt);
        if(cnt > seats){
            while(cnt > seats){
                res += cnt;
                cnt -= seats;
            }
        }
        res += cnt;
   }
   return res;
};