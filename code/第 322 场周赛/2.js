/**
 * @param {number[]} skill
 * @return {number}
 */
 var dividePlayers = function(skill) {
     let sum = 0,map = {};
     for(let i = 0; i < skill.length; i++){
        map[skill[i]] = (map[skill[i]] || 0) + 1;
        sum += skill[i];
     }
     const tn = skill.length / 2;
     if(sum % tn != 0) return -1;
     sum /= tn;
    //  console.log('sum: ', sum);
     let res = 0;
    //  console.log('map: ', map);
     for(let key in map){
         if(map[sum - key] != map[key]) return -1;
         let num = map[key];
         if(sum - key == key) num /= 2;
         res += (sum - key) * key * num;
        //  console.log(sum - key,key,map[key],(sum - key) * key * map[key]);
         delete map[key];
         delete map[sum - key];
     }
     return res;
};