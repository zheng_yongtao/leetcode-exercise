/**
 * @param {string} sentence
 * @return {boolean}
 */
 var isCircularSentence = function(sentence) {
    sentence = sentence.split(' ');
    for(let i = 0; i < sentence.length; i++){
        const next = sentence[i + 1] || sentence[0];
        if(sentence[i][sentence[i].length - 1] != next[0]) return false;
    }
    return true;
};