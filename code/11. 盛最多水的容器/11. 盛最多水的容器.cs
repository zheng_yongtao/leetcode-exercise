﻿using System;


namespace LeetCode_Exercise
{
    public class Solution
    {
        public int MaxArea(int[] height)
        {
            // 双指针
            int n = height.Length, max = -1;
            int l = 0, r = n - 1;

            while (l < r)
            {
                // 面积为矮的指针乘以长度，每次循环改变矮的指针
                if (height[l] < height[r])
                {
                    max = Math.Max(height[l] * (r - l), max);
                    l++;
                }
                else
                {
                    max = Math.Max(height[r] * (r - l), max);
                    r--;
                }

            }

            return max;
        }
    }
}
