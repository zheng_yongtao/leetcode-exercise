/**
 * @param {number} n
 * @return {number}
 */
var pivotInteger = function(n) {
    let sum = 0;
    for(let i = 0; i <= n; i++){
        sum += i;
    }
    let c = 0;
    for(let i = 0; i <= n; i++){
        c += i;
        if(c == (sum - c + i)) return i;
    }
    return -1;
};