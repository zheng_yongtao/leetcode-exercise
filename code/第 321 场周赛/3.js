
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var removeNodes = function(head) {
    const list = [];
    while(head){
        list.push(head.val);
        head = head.next;
    }
    const arr = [];
    for(let i = 0; i < list.length; i++){
        while(arr[arr.length - 1] < list[i]) arr.pop();
        arr.push(list[i]);
    }
    let ind = 0;
    const root = new ListNode(arr[ind++]);
    let r = root;
    while(arr.length > ind){
        r.next = new ListNode(arr[ind++]);
        r = r.next;
    }
    // console.log(arr);
    return root;
};