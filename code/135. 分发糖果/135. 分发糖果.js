/**
 * @param {number[]} ratings
 * @return {number}
 */
 var candy = function(ratings) {
    let ans = new Array(ratings.length).fill(1);
    let res = 0;
    for(let i = 1; i < ratings.length; i++){
        if(ratings[i] > ratings[i - 1]){
           ans[i] = ans[i - 1] + 1;
        }
    }
    for(let i = ratings.length - 1; i >= 0; i--){
        if(i != ratings.length - 1 && ratings[i] > ratings[i + 1]){
            ans[i] = Math.max(ans[i + 1] + 1,ans[i]);
        }
        res += ans[i];
    }
    return res;
};