/**
 * @param {number[]} temperatureA
 * @param {number[]} temperatureB
 * @return {number}
 */
 var temperatureTrend = function(temperatureA, temperatureB) {
     let arr1 = [],arr2 = [];
     const cal = (a,b)=>{
         if(a == b) return 0;
         return a - b > 0 ? 1 : -1;
     };
     for(let i = 1; i < temperatureA.length; i++){
        arr1.push(cal(temperatureA[i],temperatureA[i - 1]));
        arr2.push(cal(temperatureB[i],temperatureB[i - 1]));
     }
     let res = 0,temp = 0;
     for(let i = 0; i < arr1.length; i++){
         if(arr1[i] == arr2[i]) temp++;
         else temp = 0;
         res = Math.max(res,temp);
     }
     return res;
};