/**
 * @param {number} num
 * @param {string[]} plate
 * @return {number[][]}
 */
 var ballGame = function(num, plate) {
    const arr = [];
    const res = [];
    const p = [];
    const map = new Map();
    plate.map(item=>{
        arr.push(item.split(''));
    })
    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arr[i].length; j++){
            if(arr[i][j] == 'O'){
                p.push([i,j]);
            }
        }
    }
    //左上右下
    const dirx = [-1, 0, 1, 0],diry = [0, 1, 0, -1];
    const dfs = (x,y,dir,pre,step) => {
        // console.log('x,y,dir,pre,step: ', x,y,dir,pre,step);
        if(x < 0 || y < 0 || x >= arr.length || y >= arr[0].length) return;
        if(step > num) return;
        if(arr[x][y] == 'E'){
            const d = (dir + 1) % 4;
            dfs(x + dirx[d],y + diry[d],d,pre,step + 1);
        }else if(arr[x][y] == 'W'){
            const d = (dir + 3) % 4;
            dfs(x + dirx[d],y + diry[d],d,pre,step + 1);
        }else if(arr[x][y] == 'O'){
            res.push(pre);
            return;
        }else{
            dfs(x + dirx[dir],y + diry[dir],dir,pre,step + 1);
        }
    };
     // console.log(arr);
     // dfs(7,2,1,[7,2],0);
    for(let i = 1; i < arr[0].length - 1; i++){
        if(arr[0][i] != 'O') dfs(0,i,2,[0,i],0);
        if(arr[arr.length - 1][i] != 'O') dfs(arr.length - 1,i,0,[arr.length - 1,i],0);
    }
    for(let i = 1; i < arr.length - 1; i++){
        if(arr[i][0] != 'O') dfs(i,0,1,[i,0],0);
        if(arr[i][arr[0].length - 1] != 'O') dfs(i,arr[0].length - 1,3,[i,arr[0].length - 1],0);
    }
    console.log(res.length);
    return res;
};