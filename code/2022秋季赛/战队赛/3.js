/**
 * @param {number[]} flowers
 * @param {number} cnt
 * @return {number}
 */
 var beautifulBouquet = function(flowers, cnt) {
    const MOD = 1e9 + 7;
    let left = 0,right = 0;
    let m = {};
    const C = (n,m)=>{
        return n - m + 1;
    }
    let res = 0;
    for(let i = 0; i < cnt; i++) res += C(flowers.length,i + 1);
    console.log('res: ', res);
    for(let i  = 0; i < flowers.length; i++){
        if(m[flowers[i]] == cnt){
            console.log(i,left,right,res);
            for(let j = cnt + 1; j <= i - left; j++){
                res += C(i - left,j) % MOD;
            }
            for(let j = cnt + 1; j < right - left; j++){
                res -= C(right - left,j);
            }
            right = i;
            while(m[flowers[i]] == cnt){
                m[flowers[left]]--;
                left++;
            }
        }
        m[flowers[i]] = (m[flowers[i]] || 0) + 1;
    }
    let len = flowers.length - 1;
    if(m[flowers[len]] <= cnt){
       for(let j = cnt + 1; j <= len + 1 - left; j++){
           res += C(len + 1 - left,j) % MOD;
       }
       for(let j = cnt + 1; j < right - left; j++){
           res -= C(right - left,j);
       }
    }
    return res;
};