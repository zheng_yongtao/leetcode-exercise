/**
 * @param {string[]} demand
 * @return {number}
 */
 var minNumBooths = function(demand) {
     let arr = [];
     for(let i = 0; i < demand.length; i++){
         let m = {};
         for(let j = 0; j < demand[i].length; j++){
             m[demand[i][j]] = (m[demand[i][j]] || 0) + 1;
         }
         arr.push(m);
     }
     let res = 0;
     let map = {};
     console.log('arr: ', arr);
     for(let i = 0; i < arr.length; i++){
         for(let k in arr[i]){
             if(!map[k]) map[k] = arr[i][k];
             else{
                 map[k] = Math.max(map[k],arr[i][k]);
             }
         }
     }
     console.log('map',map);
     for(let k in map){
         res += map[k];
     }
     return res;
};