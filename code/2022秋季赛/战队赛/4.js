/**
 * @param {string[]} words
 * @return {number}
 */
 var Leetcode = function(words) {
     let copyWords = [... words];
     const str = 'helloleetcode';
     let strMap = {};
     let numMap = {};
     let res = 0;
     let minRes = 0;
     for(let i = 0; i < str.length; i++){
        strMap[str[i]] = (strMap[str[i]] || 0) + 1;
        numMap[str[i]] = [];
     }
     for(let i = 0; i < words.length; i++){
         words[i] = words[i].split('');
         for(let j = 0; j < words[i].length; j++){
             if(numMap[words[i][j]]){
                //  console.log('words[i][j]',words[i],words[i][j],j,words[i].length - j - 1);
                numMap[words[i][j]].push(j * (words[i].length - j - 1));
                words[i].splice(j,1);
                j--;
             }
         }
     }
     let resMap1 = {};
     for(let key in numMap){
         let conut = 0;
         let arr = numMap[key].sort((a,b)=>{
             return a - b;
         })
         for(let i = 0; i < strMap[key];i++){
             res += arr[i];
             conut += arr[i];
         }
         resMap1[key] = conut;
     }
     minRes = res;
      console.log('numMap',numMap,strMap);
     console.log('res: ', res);
     
     strMap = {};
     numMap = {};
     for(let i = 0; i < str.length; i++){
        strMap[str[i]] = (strMap[str[i]] || 0) + 1;
        numMap[str[i]] = [];
     }
     words = copyWords;
     for(let i = 0; i < words.length; i++){
         words[i] = words[i].split('');
         for(let j = words[i].length - 1; j >= 0; j--){
             if(numMap[words[i][j]]){
                //  console.log('words[i][j]',words[i],words[i][j],j,words[i].length - j - 1);
                numMap[words[i][j]].push(j * (words[i].length - j - 1));
                words[i].splice(j,1);
                j++;
             }
         }
     }
     let resMap = {};
     for(let key in numMap){
         let count = 0;
         let arr = numMap[key].sort((a,b)=>{
             return a - b;
         })
         for(let i = 0; i < strMap[key];i++){
             res += arr[i];
             count += arr[i];
         }
         resMap[key] = count;
     }
     res = 0;
     for(let k in strMap){
         res += Math.min(resMap[k],resMap1[k]);
     }
    //  console.log('numMap',numMap,strMap);
     console.log('resMap',resMap,resMap1);
    //  console.log('numMap',numMap,strMap);
    return res;
};
let words = ["hello","leetcode"];
// words = ["hold","engineer","cost","level"];
words = ["axer","qsuec","rg","cod","lauefxbv","oexyzjr","yefttp","gbnpaccl","lj","kineyykk","esecokfl","qlf","wuxahozg","z","py","ohqpea","nwrtt","ixmvpbsw","jixygsly","cqiudy"];
console.log(Leetcode(words));