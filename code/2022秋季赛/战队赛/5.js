/**
 * @param {number} size
 * @return {number[][]}
 */
 var sandyLandManagement = function(size) {
     let res = [[1,1]];
     for(let i = 1; i < size; i++){
         res.push([i + 1,1],[i + 1,(i + 1) * 2 - 1]);
     }
     return res;
};