/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
 var expandBinaryTree = function(root) {
     const dfs = (r)=>{
         if(!r) return;
         if(r.val != -1){
            const leftNode = new TreeNode(-1,r.left,null);
            const rightNode = new TreeNode(-1,r.right,null);
            if(r.left){
                dfs(r.left);
                 r.left = leftNode;
            }
            if(r.right){
                dfs(r.right);
                r.right = rightNode;
            }
         }
     }
     dfs(root);
     return root;
};