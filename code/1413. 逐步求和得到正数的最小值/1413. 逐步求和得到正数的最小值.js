/**
 * @param {number[]} nums
 * @return {number}
 */
 var minStartValue = function(nums) {
    let res = 1,base = 0;
    nums.map(num=>{
        base += num;
        res = Math.min(res,base);
    });
    return Math.max(-res + 1,1);
};