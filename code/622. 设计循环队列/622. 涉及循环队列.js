
/**
 * @param {number} k
 */
 var MyCircularQueue = function(k) {
    this.head = 0;
    this.tail = -1;
    this.size = k;
    this.list = new Array(k).fill(-1);
};

/** 
 * @param {number} value
 * @return {boolean}
 * enQueue(value): 向循环队列插入一个元素。如果成功插入则返回真。
 */
MyCircularQueue.prototype.enQueue = function(value) {
    if(this.isFull()) return false;
    const ind = (this.tail + 1) % this.size;
    this.list[ind] = value;
    this.tail = ind;
    return true;
};

/**
 * @return {boolean}
 * deQueue(): 从循环队列中删除一个元素。如果成功删除则返回真。
 */
MyCircularQueue.prototype.deQueue = function() {
    if(this.isEmpty()) return false;
    this.list[this.head] = -1;
    const ind = (this.head + 1) % this.size;
    if(this.list[ind] != -1) this.head = ind;
    else{
        this.head = 0;
        this.tail = -1;
    }
    return true;
};

/**
 * @return {number}
 * Front: 从队首获取元素。如果队列为空，返回 -1 。
 */
MyCircularQueue.prototype.Front = function() {
    return this.list[this.head];
};

/**
 * @return {number}
 * Rear: 获取队尾元素。如果队列为空，返回 -1 。
 */
MyCircularQueue.prototype.Rear = function() {
    return this.tail == -1 ? -1 : this.list[this.tail];
};

/**
 * @return {boolean}
 */
MyCircularQueue.prototype.isEmpty = function() {
    return this.list[this.head] == -1;
};

/**
 * @return {boolean}
 */
MyCircularQueue.prototype.isFull = function() {
    return this.head == (this.tail + 1) % this.size && this.list[this.head] != -1
};

MyCircularQueue.prototype.console = function(){
    return `head:${this.head},tail:${this.tail},list:${this.list}`;
}

/**
 * Your MyCircularQueue object will be instantiated and called as such:
 * var obj = new MyCircularQueue(k)
 * var param_1 = obj.enQueue(value)
 * var param_2 = obj.deQueue()
 * var param_3 = obj.Front()
 * var param_4 = obj.Rear()
 * var param_5 = obj.isEmpty()
 * var param_6 = obj.isFull()
 */
let oprate = ["MyCircularQueue","enQueue","Rear","Rear","deQueue","enQueue","Rear","deQueue","Front","deQueue","deQueue","deQueue"];
let params = [[6],[6],[],[],[],[5],[],[],[],[],[],[]];
oprate = ["MyCircularQueue","enQueue","deQueue","Front","deQueue","Front","Rear","enQueue","isFull","deQueue","Rear","enQueue"];
params = [[3],[7],[],[],[],[],[],[0],[],[],[],[3]];
oprate = ["MyCircularQueue","enQueue","enQueue","enQueue","enQueue","deQueue","deQueue","isEmpty","isEmpty","Rear","Rear","deQueue"];
params = [[8],[3],[9],[5],[0],[],[],[],[],[],[],[]];
oprate.shift();
const q = new MyCircularQueue(...params.shift())
while(oprate.length){
    const opt = oprate.shift();
    const param = params.shift();
    console.log('oprate',opt);
    console.log(q[opt](param));
    console.log(q.console());
}