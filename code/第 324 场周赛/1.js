/**
 * @param {string[]} words
 * @return {number}
 */
var similarPairs = function(words) {
    let res = 0;
    for(let i = 0; i < words.length; i++){
        let word = words[i];
        words[i] = [...new Set(word.split(''))].sort().join('');
    }
    for(let i = 0; i < words.length; i++){
        for(let j = i + 1; j < words.length; j++){
            if(words[i] == words[j]) res++;
        }
    }
    return res;
};

let words = ["aba","aabb","abcd","bac","aabc"];
console.log(similarPairs(words));