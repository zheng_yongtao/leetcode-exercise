/**
 * @param {number} n
 * @return {number}
 */
 var smallestValue = function(n) {
    const on = n;
     let nums = new Array(100005).fill(true);
     nums[0] = false;
     nums[1] = false;
     for(let i = 2; i < nums.length; i++){
         if(nums[i]){
             for(let j = 2; j * i < nums.length; j++){
                 nums[i * j] = false;
             }
         }
     }
     const doMod = (n)=>{
         for(let i = 2; i < n; i++){
             if(nums[i] && n % i == 0){
                 if(nums[n / i]) return i + n / i;
                 else{
                     if(doMod(n / i)) return i + doMod(n / i);
                 }
             }
         }
         return false;
     };
     let res = 0;
     while(1){
         let nn = doMod(n);
         if(nn == n){
            if(res == 0 && nums[on]) return on;
            return nn;
         }
         n = nn;
         if(n == false) break;
         res = n;
     }
     if(res == 0 && nums[on]) return on;
     return res;
};
let n = 15;
// n = 4;
console.log(smallestValue(n));