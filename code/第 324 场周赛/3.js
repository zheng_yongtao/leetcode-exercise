/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
 var isPossible = function(n, edges) {
    const map = new Map();
    const set = new Set();
    for(let i = 0; i < edges.length; i++){
       map.set(edges[i][0],(map.get(edges[i][0]) || 0) + 1);
       map.set(edges[i][1],(map.get(edges[i][1]) || 0) + 1);
       if(edges[i][0] > edges[i][1]){
           set.add(edges[i][1] + '-' + edges[i][0]);
       }else{
           set.add(edges[i][0] + '-' + edges[i][1]);
       }
    }
    const arr = [];
    for(let i = 1; i <= n; i++){
        if(map.get(i) % 2 == 1) arr.push(i);
    }
    if(arr.length == 0) return true;
    if(arr.length > 4 || arr.length % 2 == 1) return false;
    const check = (a,b)=>{
        if(!set.has(a + '-' + b)) return true;
        for(let k = 1; k <= n; k++){
            if(arr.includes(k)) continue;
            const a1 = Math.min(k,a);
            const b1 = Math.max(k,a);
            const a2 = Math.min(k,b);
            const b2 = Math.max(k,b);
            if(!set.has(a1 + '-' + b1) && !set.has(a2 + '-' + b2)) return true;
        }
        return false;
    };
    if(arr.length == 2){
        return check(arr[0],arr[1]);
    }
    const x1 = check(arr[0],arr[1]) && check(arr[2],arr[3]);
    const x2 = check(arr[0],arr[2]) && check(arr[1],arr[3]);
    const x3 = check(arr[0],arr[3]) && check(arr[1],arr[2]);
    return x1 || x2 || x3;
};