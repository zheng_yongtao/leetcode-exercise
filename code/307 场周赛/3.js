/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} start
 * @return {number}
 */
 var amountOfTime = function(root, start) {
    let lD = 0,rD = 0, nodes = [];
    let dfs = function(r,d,flag = false,first = false){
       if(!r) return; 
       if(r.val == start){
            nodes.push({
                flag:flag,
                d: d
            });
        }
        if(flag){
            lD = Math.max(lD,d);
        }else{
            rD = Math.max(rD,d);
        }
        if(r) dfs(r.left,d+1,first || flag,false);
        if(r) dfs(r.right,d+1,!first || flag,false);
    }
    dfs(root,0,false,true);
    console.log(nodes,lD,rD);
    let res = 0;
    for(let )
    return 0;
};