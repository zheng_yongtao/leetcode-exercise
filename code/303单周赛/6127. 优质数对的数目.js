/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var countExcellentPairs = function(nums, k) {
    // 单个字符考虑30位
    const MAX = 30;
    // 去重
    nums = [...new Set(nums)]
    // 获取位数1的个数
    const getOneCount = (num) => {
        let ans = 0;
        while (num) {
            if (num & 1 === 1) {
                ans++;
            }
            num >>= 1;
        }
        return ans;
    }
    // 统计位数1个数相同的数量
    const cnt = new Array(MAX + 1).fill(0)
    for (let i = 0; i < nums.length; i++) {
        cnt[getOneCount(nums[i])]++;
    }
    
    let ans = 0;
    // 枚举，当满足条件时，能组成的数目是数量乘积
    for (let i = 0; i <= MAX; i++) {
        for (let j = 0; j <= MAX; j++) {
            if ((i + j) >= k) {
                ans += (cnt[i] * cnt[j])
            }
        }
    }
    
    return ans;
};