/**
 * @param {number[][]} grid
 * @return {number}
 */
 var equalPairs = function(grid) {
     const n = grid.length;
     let res = 0;
     const col = [],row = [];
     for(let i = 0; i < n; i++){
         col.push(grid[i].join('-'));
         let t = '';
         for(let j = 0; j < n; j++){
             if(t != '') t += '-';
             t += grid[j][i];
         }
         row.push(t);
     }
     for(let i = 0; i < n; i++){
         for(let j = 0; j < n; j++){
             if(col[i] == row[j]){
                res++;
             }
         }
     }
     return res;
};