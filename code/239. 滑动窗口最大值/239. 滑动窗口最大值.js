/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
 var maxSlidingWindow = function(nums, k) {
    const q = [];
    for(let i = 0; i < k; i++){
        while(q.length && nums[q[q.length - 1]] <= nums[i]) q.pop();
        q.push(i);
    }
    const res = [];
    for(let i = k; i < nums.length; i++){
        res.push(nums[q[0]]);
        if(i - k >= q[0]) q.shift();
        while(q.length && nums[q[q.length - 1]] <= nums[i]) q.pop();
        q.push(i);
    }
    res.push(nums[q[0]]);
    return res;
};