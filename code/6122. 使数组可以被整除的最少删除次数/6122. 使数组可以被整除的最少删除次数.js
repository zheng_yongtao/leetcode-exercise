/**
 * @param {number[]} nums
 * @param {number[]} numsDivide
 * @return {number}
 */
 var minOperations = function(nums, numsDivide) {
    const gcd = function(a,b){
    return a == 0 ? b : gcd(b % a, a);
    };
    let g = 0;
    for (let  x of numsDivide) g = gcd(g, x);
    nums.sort((a,b)=>a-b);
    for (let i = 0; i < nums.length; i++)
        if (g % nums[i] == 0) return i;
    return -1;
};




