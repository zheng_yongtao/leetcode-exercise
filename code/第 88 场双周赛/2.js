/**
 * @param {number} n
 */
 var LUPrefix = function(n) {
     this.n = n;
     this.max = 0;
     this.flag = new Array(n).fill(0);
     this.flag[0] = true;
};

/** 
 * @param {number} video
 * @return {void}
 */
LUPrefix.prototype.upload = function(video) {
    this.flag[video] = true;
    while(this.flag[this.max]){
        this.max++;
    }
};

/**
 * @return {number}
 */
LUPrefix.prototype.longest = function() {
    return Math.max(0,this.max - 1);
};

/**
 * Your LUPrefix object will be instantiated and called as such:
 * var obj = new LUPrefix(n)
 * obj.upload(video)
 * var param_2 = obj.longest()
 */