/**
 * @param {string} word
 * @return {boolean}
 */
var equalFrequency = function(word) {
     let map = {};
     for(let i = 0; i < word.length; i++){
         map[word[i]] = (map[word[i]] || 0) + 1;
     }
     map = [...Object.values(map)];
     const set = [...new Set(map)];
     return set.length == 1 && set[0] == 1;
};