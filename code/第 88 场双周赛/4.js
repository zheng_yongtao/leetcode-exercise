/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @param {number} diff
 * @return {number}
 */
 var numberOfPairs = function(nums1, nums2, diff) {
    let nums = [];
    for(let i = 0; i < nums1.length; i++){
        nums.push(nums1[i] - nums2[i]);
    }
    let res = 0;
    let l = 0;
    for(let i = 1; i < nums.length; i++){
        if( nums[l] - nums[i] > diff){
            res += (i - l) * (i - l - 1) / 2;
            l = i;
        }
    }
    const i = nums.length;
    console.log(nums,l,i,'-',nums[l],nums[i - 1],diff,nums[l] <= nums[i - 1] + diff);
    if(nums[l] <= nums[i - 1] + diff){
       res += (i - l) * (i - l - 1) / 2
    }
    return res;
};