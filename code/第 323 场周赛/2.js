/**
 * @param {number[]} nums
 * @return {number}
 */
 var longestSquareStreak = function(nums) {
    let map = {};
    for(const num of nums){
        map[num] = 1;
    }
    nums = nums.sort((a,b)=>{
        return a - b;
    });
    let res = 0;
    for(let i = 0; i < nums.length; i++){
        let num = nums[i];
        let max = 0;
        while(map[num]){
            max++;
            num *= num;
        }
        res = Math.max(res,max);
    }
    return res < 2 ? -1 : res;
};