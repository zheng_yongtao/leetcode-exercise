/**
 * @param {number} n
 */
 var Allocator = function(n) {
    this.n = n;
    this.arr = new Array(n).fill(-1);
    this.count = {};
};

/** 
 * @param {number} size 
 * @param {number} mID
 * @return {number}
 */
Allocator.prototype.allocate = function(size, mID) {
    let num = this.arr[0] == -1 ? 1 : 0;
    for(let i = 0; i < this.n; i++){
        if(i == 0){
            num = this.arr[0] == -1 ? 1 : 0;
        }else if(this.arr[i] == -1 && this.arr[i - 1] == -1){
            num++;
        }else{
            num = this.arr[i] == -1 ? 1 : 0;
        }
        if(num == size){
            let j = i;
            while(this.arr[j] == -1){
                this.arr[j] = mID;
                j--;
            }
            const count = this.count[mID] || 0;
            this.count[mID] = count + size;
            return j + 1;
        }
    }
    return -1;
};

/** 
 * @param {number} mID
 * @return {number}
 */
Allocator.prototype.free = function(mID) {
    let count = this.count[mID] || 0;
    const res = count;
    this.count[mID] = 0;
    if(count == 0) return 0;
    for(let i = 0; i < this.n; i++){
        if(this.arr[i] == mID){
            this.arr[i] = -1;
            count--;
            if(count == 0) return res;
        }
    }
};

/**
 * Your Allocator object will be instantiated and called as such:
 * var obj = new Allocator(n)
 * var param_1 = obj.allocate(size,mID)
 * var param_2 = obj.free(mID)
 */