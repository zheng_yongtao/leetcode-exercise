/**
 * @param {number[][]} grid
 * @return {number}
 */
 var deleteGreatestValue = function(grid) {
    let res = 0;
    for(let i = 0; i < grid.length; i++){
        grid[i] = grid[i].sort((a,b)=>{
            return a - b;
        });
    }
    console.log('grid',grid);
    for(let i = 0; i < grid[0].length; i++){
        let max = grid[0][i];
        for(let j = 0; j < grid.length; j++){
            max = Math.max(max,grid[j][i]);
        }
        res += max;
    }
    return res;
};