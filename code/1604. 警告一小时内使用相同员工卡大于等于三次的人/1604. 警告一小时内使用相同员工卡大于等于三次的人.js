/**
 * @param {string[]} keyName
 * @param {string[]} keyTime
 * @return {string[]}
 */
 var alertNames = function(keyName, keyTime) {
    let res = new Set();
    const map = {};
    let list = [];
    for(let i = 0; i < keyName.length; i++){
        list.push({
            keyName:keyName[i],
            keyTime:keyTime[i]
        });
    }
    list = list.sort((a,b)=>{
        a = a.keyTime.split(':');
        b = b.keyTime.split(':');
        if(a[0] == b[0]) return a[1] - b[1];
        return a[0] - b[0];
    });
    const diffTime = (time1,time2) => {
       time1 = time1.split(':');
       time2 = time2.split(':');
       let h = time2[0] - time1[0];
       let m = time2[1] - time1[1];
       return h * 60 + m;
    }
    for(let i = 0; i < list.length; i++){
        let {keyName,keyTime} = list[i];
        if(!map[keyName]){
            map[keyName] = [keyTime];
        }else{
           if(map[keyName].length >= 3) continue;
           if(map[keyName].length < 3) map[keyName].push(keyTime);
           while(map[keyName].length && diffTime(map[keyName][0],keyTime) > 60) map[keyName].shift();
           if(map[keyName].length >= 3) res.add(keyName);
        }
    }
    return [...res].sort();
};