/**
 * @param {number[]} nums
 * @return {number}
 */
var maxEqualFreq = function(nums) {
    const countMap = new Map(), numsMap = {};
    let res = 1;
    for(let i = 0; i < nums.length; i++){
        let n = numsMap[nums[i]];
        if(!n){
            numsMap[nums[i]] = 1;
            countMap.set(1,(countMap.get(1) || 0) + 1);
        }else{
            countMap.set(n,countMap.get(n) - 1);
            if(countMap.get(n) == 0) countMap.delete(n);
            n++;
            numsMap[nums[i]] = n;
            countMap.set(n,(countMap.get(n) || 0) + 1);
        }
        if(countMap.size <= 2){
            const keys = countMap.keys();
            let preK = '',preV = '';
            for(const k of keys){
                if(countMap.size == 1 && (k == 1 || countMap.get(k) == 1)) res = i + 1; 
                if(k == countMap.get(k) && k == 1) res = i + 1;
                if(!preK){
                    preK = k;
                    preV = countMap.get(k);
                }
                if((preV == 1 && preK - 1 == k) || (countMap.get(k) == 1 && k - 1 == preK)) res = i + 1;
            }
        }
    }
    return res;
};