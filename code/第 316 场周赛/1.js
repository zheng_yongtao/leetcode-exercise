/**
 * @param {string[]} event1
 * @param {string[]} event2
 * @return {boolean}
 */
 var haveConflict = function(event1, event2) {
    const start1 = parseInt(event1[0].split(':')[0]) * 60 + parseInt(event1[0].split(':')[1]);
    const end1 = parseInt(event1[1].split(':')[0]) * 60 + parseInt(event1[1].split(':')[1]);
    const start2 = parseInt(event2[0].split(':')[0]) * 60 + parseInt(event2[0].split(':')[1]);
    const end2 = parseInt(event2[1].split(':')[0]) * 60 + parseInt(event2[1].split(':')[1]);
    return (start1 <= start2 && end1 >= start2) || (start2 <= start1 && end2 >= start1) || (start1 <= end2 && end2 <= end1) || (end1 >= start2 && end1 <= end2)
};