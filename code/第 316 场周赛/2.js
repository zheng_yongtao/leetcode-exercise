/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
 var subarrayGCD = function(nums, k) {
    const gcd = (a, b) => {
        return a % b === 0 ? b : gcd(b, a % b);
    }
    const q1 = new Array(nums.length).fill(0);
    const q2 = new Array(nums.length).fill(0);
    for(let i = 0; i < q1.length; i++){
        if(i == 0){
            if(nums[i] % k == 0) q1[i] = 1;
            if(nums[q2.length - 1] % k == 0) q2[q2.length - 1] = 1;
        }else{
            if(nums[i] % k == 0) q1[i] = q1[i-1] + 1;
            if(nums[q2.length - 1 - i] % k == 0) q2[q2.length - 1 - i] = q2[q2.length - i] + 1;
        }
    }
    let res = 0;
    let flag = 0;
    for(let i = 0; i < nums.length; i++){
        if(nums[i] == k) res++;
        if(i < nums.length - 1 && gcd(nums[i],nums[i + 1]) == k){
            res += (i > 0 ? (q1[i] - flag) : 1) * (q2[i + 1] || 1);
            flag = q1[i];
        }
        if(nums[i] % k !== 0) flag = 0;
    }
    return res;
};
let nums = [3,12,9,6],k = 3;
nums = [3,3,4,1,2],k = 1;
// nums = [3,15,19,19,14],k = 1;  //8
// nums = [15,11,5,19,9,9,4], k = 1; // 20
// nums = [9,3,1,2,6,3], k = 3;
console.log(subarrayGCD(nums,k));