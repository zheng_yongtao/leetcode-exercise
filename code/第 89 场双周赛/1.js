/**
 * @param {string} time
 * @return {number}
 */
var countTime = function(time) {
    let a = 1,b = 1;
    if(time[0] === '?'){
        if(time[1] === '?') a = 24;
        else if(time[1] > 3) a = 2;
        else a = 3;
    }
    if(time[1] === '?'){
        if(time[0] === '2') a = 4;
        else if(time[0] === '?') a = 24;
        else a = 10;
    }
    if(time[3] === '?') b = 6;
    if(time[4] === '?') {
        if(time[3] === '?') b = 60;
        else b = 10;
    }
    return a * b;
};