/**
 * @param {string[]} creators
 * @param {string[]} ids
 * @param {number[]} views
 * @return {string[][]}
 */
 var mostPopularCreator = function(creators, ids, views) {
    let map = {},max = 0;
    for(let i = 0; i < creators.length; i++){
        if(map[creators[i]]){
            if(map[creators[i]].id > ids[i]){
               map[creators[i]].id = ids[i];
            }
            map[creators[i]].views = map[creators[i]].views + views[i];
            if(map[creators[i]].max < views[i] || (map[creators[i]].max == views[i] && map[creators[i]].maxid > ids[i])){
               map[creators[i]].max = views[i];
               map[creators[i]].maxid = ids[i];
            }
            max = Math.max(max,map[creators[i]].views);
        }else{
            map[creators[i]] = {};
           map[creators[i]].id = ids[i];
           map[creators[i]].views = views[i];
           map[creators[i]].max = views[i];
           map[creators[i]].maxid = ids[i];
            max = Math.max(max,views[i]);
        }
    }
    const res = [];
    for(let key in map){
        if(map[key].views == max){
            res.push([key,map[key].maxid]);
        }
    }
    return  res;
};