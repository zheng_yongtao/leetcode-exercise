/**
 * @param {number} n
 * @param {number} target
 * @return {number}
 */
 var makeIntegerBeautiful = function(n, target) {
     const getSum = (num)=>{
         let sum = 0;
         for(const s of num + ''){
             sum += parseInt(s);
         }
         return sum;
     }
     let res = 0;
     let ind = 1
     while(getSum(n) > target && ind < 20){
         console.log(Math.pow(10,ind));
         const p = Math.pow(10,ind);
         let num = n % p;
         console.log('num: ', num);
         n += p - num;
         res += p - num;
         ind ++;
     }
     console.log('n',n);
     return res;
};
let n = 16, target = 6;
n = 467, target = 6;
n = 6068060761,target = 3;
console.log(makeIntegerBeautiful(n, target));