/**
 * @param {number[]} nums
 * @return {number}
 */
 var averageValue = function(nums) {
     let sum = 0,n = 0;
     for(const num of nums){
         if(num % 6 == 0 ){
             sum += num;
             n++;
         }
     }
     return n == 0 ? 0 : Math.floor(sum / n);
};