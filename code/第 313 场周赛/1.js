/**
 * @param {number} a
 * @param {number} b
 * @return {number}
 */
 var commonFactors = function(a, b) {
     let n = Math.min(a,b);
     let res = 0;
     while(n--){
         if(a % n == 0 && b % n == 0) res++;
     }
     return res;
};