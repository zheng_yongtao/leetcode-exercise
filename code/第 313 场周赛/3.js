/**
 * @param {number} num1
 * @param {number} num2
 * @return {number}
 */
 var minimizeXor = function(num1, num2) {
    num2 = num2.toString(2);
    console.log('num2: ', num2);
    num1 = num1.toString(2);
    console.log('num1: ', num1);
     let res = new Array(num1.length).fill(0);
     let n = 0;
     for(let i = 0; i  < num2.length; i++){
         if(num2[i] === '1') n++;
     }
     for(let i = 0;i < num1.length; i++){
         if(num1[i] == '1'){
             if(n == 0) break;
            res[i] = '1';
            n--;
         }
     }
     let len = res.length - 1;
     while(n){
         while(res[len] == '1' || num1[len] == '1') len--;
         if(len < 0) break;
         res[len] = '1';
         n--
     }
     while(n--) res.unshift('1');
     console.log('res: ', res);
     res = res.join('');
     return parseInt(res,2);
};