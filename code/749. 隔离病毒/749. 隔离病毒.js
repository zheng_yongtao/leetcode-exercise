/**
 * @param {number[][]} isInfected
 * @return {number}
 */
var containVirus = function(isInfected) {
    let res = 0;
    let blocks = [],block = [];
    let dx = [0,1,0,-1],dy = [1,0,-1,0];
    const getBlock = function(x,y){
        if(x >= isInfected.length || y >= isInfected[0].length || x < 0 || y < 0) return;
        if(isInfected[x][y] == '0'){
            block.push([x,y]);
            return;
        }
        if(isInfected[x][y] > 1) return;
        isInfected[x][y] = 2;
        for(let i = 0; i < 4; i++){
            getBlock(x + dx[i], y + dy[i]);
        }
        isInfected[x][y] = 3;
    };
    for(let i = 0; i < isInfected.length; i++){
        for(let j = 0; j < isInfected[i].length; j++){
            if(isInfected[i][j] == 1){
                block = [];
                getBlock(i,j);
                blocks.push(block);
            }
        }
    }
    const goAround = (x,y,map)=>{
        let ans = [];
        for(let i = 0; i < 4; i++){
            if(map[x + dx[i]][y + dy[i]] == 0){
                ans.push(x + dx[i],y + dy[i])
                map[x + dx[i]][y + dy[i]] = 3;
            }
        }
        map[x][y] = 1;
        return ans;
    };
    let flag = new Array(blocks.length).fill(true);
    const doSplit = (arr,map)=>{
        const ans = [];
        for(let i = 0; i < arr.length; i++){
            ans.push(...goAround(arr[i][0],arr[i][1],map));
        }
        return ans
    };
    const dfs = function(map){
        for(let i = 0; i < flag.length; i++){
            if(flag[i]){
                let tempMap = JSON.parse(JSON.stringify(map));
                flag[i] = false;
                doSplit(blocks[i],tempMap);
                dfs(tempMap);
                flag[i] = true;
            }
        }
    }

    console.log(blocks);
    return res;
};

let isInfected = [[0,1,0,0,0,0,0,1],[0,1,0,0,0,0,0,1],[0,0,0,0,0,0,0,1],[0,0,0,0,0,0,0,0]];
// isInfected = [[1,1,1],[1,0,1],[1,1,1]];
console.log(containVirus(isInfected));