/**
 * @param {number[]} asteroids
 * @return {number[]}
 */
 var asteroidCollision = function(asteroids) {
     const res = [];
     for(let i = 0; i < asteroids.length; i++){
         while(res[res.length - 1] > 0 && asteroids[i] < 0 && Math.abs(res[res.length - 1]) < Math.abs(asteroids[i])){
             res.pop();
         }
         if(res.length && res[res.length - 1] > 0 && asteroids[i] < 0 ){
             if(Math.abs(res[res.length - 1]) == Math.abs(asteroids[i])){
                 res.pop();
             }
             continue;
         }else{
             res.push(asteroids[i]);
         }
     }
     return res;
};