/**
 * @param {number[]} nums
 * @return {number}
 */
 var partitionDisjoint = function (nums) {
    let max = nums[0];
    let leftMax = nums[0];
    let pos = 0;
    for (let i = 1; i < nums.length; i++) {
      max = Math.max(max, nums[i]);
      if (nums[i] < leftMax) {
        leftMax = max;
        pos = i;
      }
    }
    return pos + 1;
  };