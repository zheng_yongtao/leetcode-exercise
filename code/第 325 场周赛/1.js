/**
 * @param {string[]} words
 * @param {string} target
 * @param {number} startIndex
 * @return {number}
 */
 var closetTarget = function(words, target, startIndex) {
     let res = Infinity;
     for(let i = 0; i < words.length; i++){
         if(words[i] == target){
             res = Math.min(Math.abs(startIndex - i),words.length - Math.abs(startIndex - i),res);
         }
     }
     return res == Infinity ? -1 : res;
};
let words = ["a","b","leetcode"], target = "leetcode", startIndex = 0;
words = ["hsdqinnoha","mqhskgeqzr","zemkwvqrww","zemkwvqrww","daljcrktje","fghofclnwp","djwdworyka","cxfpybanhd","fghofclnwp","fghofclnwp"],
target = "zemkwvqrww",
startIndex = 8;
console.log(closetTarget(words, target, startIndex));