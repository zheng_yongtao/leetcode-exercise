/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
 var takeCharacters = function(s, k) {
    const map = {a:0,b:0,c:0};
    let res = Infinity;
    if(k == 0) return 0;
    if(k * 3 > s.length) return -1;
    const dfs = (l,r,map) => {
       //  console.log('l,r,map: ', l,r,map,res);
        if(l > r + 1) return;
        if(l - 1 + s.length - r >= res) return;
        if(map['a'] >= k && map['b'] >= k && map['c'] >= k){
            res = l - 1 + s.length - r;
            return;
        }
        const m = {...map};
        m[s[l]]++;
        dfs(l+1,r,m);
        const m1 = {...map};
        m1[s[r]]++;
        dfs(l,r - 1,m1);
    }
    dfs(0,s.length - 1,map);
    return res == Infinity ? -1 : res;
};
let s = "aabaaaacaabc", k = 2;
// s = "abc", k = 1;
console.log(takeCharacters(s,k));