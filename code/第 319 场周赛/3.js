/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
 var minimumOperations = function(root) {
     let map = {};
     let qq = [{r:root,f:0}];
     while(qq.length){
         let q = qq.shift();
         if(!q) continue;
         if(!map[q.f]) map[q.f] = [];
         map[q.f].push(q.r.val);
         if(q.r.left){
            qq.push({r:q.r.left,f:q.f+1});
         }
         if(q.r.right){
            qq.push({r:q.r.right,f:q.f+1});
         }
     }
     let res = 0;
     for(let k in map){
         let min = Math.min(...map[k]);
         let len = map[k].length;
         for(let i = 0; i < map[k].length; i++){
             if(map[k][i] == (min + i)) len--;
         }
         res += Math.max(len - 1,0);
         console.log(min,map[k],len);
     }
     console.log(map);
     return res;
};