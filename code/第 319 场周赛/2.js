/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
 var subarrayLCM = function(nums, k) {
    let res = 0;
    const gcd = (a,b)=>{
        let temp;
        while(b != 0){
            temp = a % b;
            a = b;
            b = temp;
        }
        return a;
    };
    const scm = (a,b) => {
        return a * b / gcd(a,b);
    };
    for(let i = 0; i < nums.length; i++){
        let flag = [];
        let startI = i;
        while(i < nums.length && k % nums[i] == 0){
            if(nums[i] == k) res++;
            if(i + 1 < nums.length && scm(nums[i],nums[i+1]) == k){
                flag.push(i);
            }
            i++;
        }
        for(let j = 0; j < flag.length; j++){
            res += Math.max(1,flag[j] + 1 - startI);
            res += Math.max((flag[j + 1] || (i - 1)) - flag[j] - 1,0);
        }
        console.log(startI,flag,i,res);
    }
    return res;
};
let nums = [2,1,1,5],k = 5;
console.log(subarrayLCM(nums,k));