/**
 * @param {number[]} nums
 * @return {number}
 */
 var findMaxK = function(nums) {
     let map = {};
     for(const num of nums){
        map[num] = true;
     }
     nums = nums.sort((a,b)=>{
         return b - a;
     });
     for(const num of nums){
         if(map[num * -1]){
             return num;
         }
     }
     return -1;
};