/**
 * @param {number[]} nums
 * @param {number} minK
 * @param {number} maxK
 * @return {number}
 */
 var countSubarrays = function(nums, minK, maxK) {
    let l = 0,r = 0,minInd = -1,maxInd = -1,res = 0;
    for(let i = 0; i < nums.length; i++){
        if(nums[i] == minK) minInd = i;
        if(nums[i] == maxK) maxInd = i;
        if(maxK < nums[i] || minK > nums[i]) l = i + 1;
        res += Math.max(0,Math.min(minInd,maxInd) - l + 1);
    }
    return res;
};

let nums = [1,3,5,2,7,5], minK = 1, maxK = 5;
nums = [1,1,1,1], minK = 1, maxK = 1;
nums = [35054,398719,945315,945315,820417,945315,35054,945315,171832,945315,35054,109750,790964,441974,552913];
minK = 35054;
maxK = 945315;
console.log(countSubarrays(nums, minK, maxK));