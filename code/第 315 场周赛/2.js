/**
 * @param {number[]} nums
 * @return {number}
 */
 var countDistinctIntegers = function(nums) {
     let set = new Set();
     const getReverse = (num)=>{
        if(num == 0) return 0;
        let res = '';
        while(num > 0){
           res += num % 10;
           num = Math.floor(num / 10);
        }
        return parseInt(res);
    };
    for(const num of nums){
        set.add(num + '');
        set.add(getReverse(num) + '');
    }
    // console.log([...set]);
    return set.size;
};
let nums = [1,13,10,12,31];
console.log(countDistinctIntegers(nums));