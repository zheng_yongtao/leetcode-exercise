/**
 * @param {number} num
 * @return {boolean}
 */
 var sumOfNumberAndReverse = function(num) {
     const getReverse = (num)=>{
         if(num == 0) return 0;
         let res = '';
         while(num){
            res += num % 10;
            num = Math.floor(num / 10);
         }
         return res;
     };
     for(let i = 0; i <= num; i++){
        //  console.log(i,parseInt(getReverse(i)),i + parseInt(getReverse(i)));
         if(i + parseInt(getReverse(i)) == num){
            // console.log(i,parseInt(getReverse(i)),i + parseInt(getReverse(i)));
            return true;
         }
     }
     return false;
};
let num = 2;
console.log(sumOfNumberAndReverse(num));