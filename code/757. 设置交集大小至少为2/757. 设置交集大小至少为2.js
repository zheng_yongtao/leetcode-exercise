/**
 * @param {number[][]} intervals
 * @return {number}
 */
 var intersectionSizeTwo = function(intervals) {
    intervals.sort((a, b) => a[1] == b[1] ? b[0] -a[0] : a[1] - b[1]);
    const res = new Set();
    res.add(intervals[0][1]);
    res.add(intervals[0][1] - 1);
    let a = intervals[0][1] - 1,b = intervals[0][1];
    for(let i = 1; i < intervals.length; i++){
        if(intervals[i][0] > a && intervals[i][0] <= b){
            [a,b] = [b,intervals[i][1]];
            res.add(b);
        }else if(intervals[i][0] > b){
            [a,b] = [intervals[i][1] - 1,intervals[i][1]];
            res.add(a);
            res.add(b);
        }
    }
    return res.size;
};