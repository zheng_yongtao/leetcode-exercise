/**
 * @param {string} s
 * @return {number}
 */
 var longestContinuousSubstring = function(s) {
    let res = 1;
    let count = 1;
    for(let i = 1; i < s.length; i++){
       //  console.log(s[i],s[i - 1],s[i].charCodeAt());
        if(s[i].charCodeAt() == (s[i - 1].charCodeAt() + 1)){
            count++;
        }else{
            count = 1;
        }
        res = Math.max(res,count);
    }
    return res;
};