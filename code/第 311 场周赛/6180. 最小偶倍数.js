/**
 * @param {number} n
 * @return {number}
 */
 var smallestEvenMultiple = function(n) {
     for(let i = 1;i > 0; i++){
         if((i * n) % 2 == 0) return i * n;
     }
};