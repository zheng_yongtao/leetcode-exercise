/**
 * @param {string[]} words
 * @return {number[]}
 */
 var sumPrefixScores = function(words) {
     let tree = {};
     words.map(word=>{
         let t = tree;
         for(const w of word){
             if(!t[w]){
                 t[w] = {count:1};
             }else{
                 t[w].count++;
             }
             t = t[w];
         }
     })
     let res = new Array(words.length).fill(0);
     for(let i = 0; i < words.length; i++){
         let t = tree;
         for(const w of words[i]){
            res[i] += t[w].count;
            t = t[w];
         }
     }
     return res;
};