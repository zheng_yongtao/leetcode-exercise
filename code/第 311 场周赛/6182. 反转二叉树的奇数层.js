/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
 var reverseOddLevels = function(root) {
    let res = [];
    let q = [{q:root,f:0}];
    while(q.length){
        const t = q.pop();
        if(res.length > t.f){
            t.f % 2 == 1 ? res[t.f].push(t.q.val) : res[t.f].unshift(t.q.val);
        }else{
            res.push([t.q.val]);
        }
        t.q.left && q.push({q:t.q.left,f:t.f+1});
        t.q.right && q.push({q:t.q.right,f:t.f+1});
    }
    res = res.flat();
    let ans = new TreeNode(res.shift());
    let ta = [ans];
    while(ta.length){
        let tb = ta.shift();
        if(res.length){
            tb.left = new TreeNode(res.shift());
            tb.right = new TreeNode(res.shift());
            ta.push(tb.left,tb.right);
        }
    }
    return ans;
};