
/**
 * @param {string} s1
 * @param {string} s2
 * @return {string[]}
 */
 var uncommonFromSentences = function(s1, s2) {
    const map = {};
    s1.split(' ').forEach(s=>{
        map[s] = (map[s] || 0) + 1;
    });
    s2.split(' ').forEach(s=>{
        map[s] = (map[s] || 0) + 1;
    });
    const res = [];
    for(const key in map){
        if(map[key] === 1) res.push(key);
    }
    return res;
};