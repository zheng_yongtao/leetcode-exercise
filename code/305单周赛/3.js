/**
 * @param {number[]} nums
 * @return {boolean}
 */
 var validPartition = function(nums) {
     let flag = new Array(nums.length).fill(0);
     for(let i = 1; i < nums.length; i++){
         if(nums[i] == nums[i - 1]){
             flag[i] = flag[i - 1] + 1;
         }
     }
     for(let i = nums.length - 2; i >= 0; i--){
         if(nums[i] == nums[i + 1]){
             flag[i] = flag[i + 1];
         }
     }
     for(let i = 1; i < nums.length; i++){
         if(nums[i] != nums[i - 1]){
                if((flag[i - 1] > 2 && (nums[i] != nums[i - 1] + 1 || nums[i] != nums[i + 1] - 1)) && (flag[i - 1] > 2 && (nums[i] != nums[i - 1] + 1 || nums[i] != nums[i + 1] - 1)))
         }
     }
};
let nums = [4,4,4,5,6,7];
// nums = [1,1,1,2];
// nums = [1,1,2,3,4];
console.log(validPartition(nums));