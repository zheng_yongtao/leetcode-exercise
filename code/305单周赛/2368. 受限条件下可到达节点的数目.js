class UnionSet {
    constructor(n) {
        this.fa = new Array(n + 1);
        for (let i = 0; i <= n; i++) {
            this.fa[i] = i;
        }
    }
    get(x) {
        return (this.fa[x] = this.fa[x] == x ? x : this.get(this.fa[x]));
    }
    merge(a, b) {
       this.fa[this.get(a)] = this.get(b);
    }
}
/**
 * @param {number} n
 * @param {number[][]} edges
 * @param {number[]} restricted
 * @return {number}
 */
var reachableNodes = function (n, edges, restricted) {
    const mask = new Set();
    for (const r of restricted) {
        mask.add(r);
    }
    const u = new UnionSet(n);
    for (const [a, b] of edges) {
        if (mask.has(a) || mask.has(b)) continue;
        u.merge(a, b);
    }
    let ans = 1;
    // “0”所在集合的根结点
    const fa = u.get(0);
    for (let i = 1; i < n; i++) {
        if (u.get(i) === fa) ans++;
    }
    return ans;
};