/**
 * @param {number[]} nums
 * @param {number} diff
 * @return {number}
 */
var arithmeticTriplets = function(nums, diff) {
    let res = 0;
    for(let i = 0; i < nums.length; i++){
        let j = i + 1, k = nums.length - 1;
        while(j < k){
            if(nums[j] - nums[i] == diff && nums[k] - nums[j] == diff) res++;
            if(nums[j] - nums[i] > diff || nums[k] - nums[j] < diff) break;
            if(nums[j] - nums[i] < diff) j++;
            else k--;
        }
    }
    return res;
};