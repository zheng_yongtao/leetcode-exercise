/**
 * @param {number} n
 * @return {number}
 */
 var countSpecialNumbers = function(n) {
     let res = [9];
     let s = 9;
     for(let i = 9; i >= 1; i--){
         s *= i;
         res.push(s);
     }
     let ans = 0;
     n += '';
     let set = new Set();
     for(let i = 0; i < n.length - 1; i++){
         ans += (n[i] - set.size) * res[n.length - 2 - i];
         set.add(n[i]);
     }
     ans += parseInt(n) % 10 + (n.length == 1 ? 0 : 1);
     return ans;
};
let n = 20;
n = 5;
n = 135;
console.log(countSpecialNumbers(n));