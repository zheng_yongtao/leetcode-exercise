/**
 * @param {number[][]} grid
 * @return {number[][]}
 */
 var largestLocal = function(grid) {
     let res = new Array(grid.length - 2);
     for(let i = 0; i < grid.length - 2; i++){
         for(let j = 0; j < grid[i].length - 2; j++){
             let max = grid[i][j];
             for(let k = i; k < i + 3; k++){
                 for(let l = j; l < j + 3; l++){
                     max = Math.max(grid[k][l],max);
                 }
             }
             if(!res[i]) res[i] = [];
             res[i].push(max);
         }
     }
     return res;
};