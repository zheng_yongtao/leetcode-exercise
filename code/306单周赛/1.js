/**
 * @param {number[]} edges
 * @return {number}
 */
 var edgeScore = function(edges) {
     let map = {},max = 0,res = 0;
     for(let i = 0; i < edges.length; i++){
         map[edges[i]] = (map[edges[i]] || 0) + i;
         if(map[edges[i]] > max){
             max = map[edges[i]];
             res = edges[i];
         }else if(map[edges[i]] == max){
             if(res > edges[i]){
                 res = edges[i];
             }
         }
     }
     return res;
};