/**
 * @param {number[]} groupSizes
 * @return {number[][]}
 */
 var groupThePeople = function(groupSizes) {
    const res = [],map = {};
    for(let i = 0; i < groupSizes.length; i++){
        let ind = map[groupSizes[i]];
        if(ind === void 0 || res[ind].length === groupSizes[i]){
            map[groupSizes[i]] = res.length;
            res.push([i]);
        }else{
            res[ind].push(i);
        }
    }
    return res;
};
let groupSizes = [3,3,3,3,3,1,3];
console.log(groupThePeople(groupSizes));