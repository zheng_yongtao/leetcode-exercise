/**
 * @param {string} s
 * @return {number}
 */
 var partitionString = function(s) {
    let map = {};
    let res = 0;
    for(let i = 0; i < s.length; i++){
        if(!map[s[i]]){
            map[s[i]] = true;
        }else{
            map = {};
            map[s[i]] = true;
            res++;
        }
    }
    return res + 1;
};