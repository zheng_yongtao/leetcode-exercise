/**
 * @param {number[][]} intervals
 * @return {number}
 */
 var minGroups = function(intervals) {
     let arr = new Array(1e6 + 5).fill(0);
     for(let i = 0; i < intervals.length; i++){
         arr[intervals[i][0]]++;
         arr[intervals[i][1] + 1]--;
     }
     let res =  arr[0];
     for(let i = 1; i < arr.length; i++){
         arr[i] += arr[i - 1];
         res = Math.max(arr[i],res);
     }
     return res;
};