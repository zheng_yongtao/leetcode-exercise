/**
 * @param {number[]} nums
 * @return {number}
 */
 var mostFrequentEven = function(nums) {
    let res = -1;
    let map = {};
    for(let i = 0; i < nums.length; i++){
        map[nums[i]] = (map[nums[i]] || 0) + 1;
    }
    let max = 0;
    for(let k in map){
        if(k % 2 == 0 && map[k] >= max){
            if(map[k] == max) res = Math.min(res,k);
            else{
                res = k;
                max = map[k];
            }
        }
    }
    return res;
};