/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var numberOfPairs = function(nums) {
    let map = {};
    for(let i = 0; i < nums.length; i++){
        map[nums[i]] = (map[nums[i]] || 0) + 1;
    }
    let res = 0;
    let ans = 0;
    for(let k in map){
        ans += Math.floor(map[k] / 2);
        res += map[k] % 2;
    }
    return [ans,res];
};