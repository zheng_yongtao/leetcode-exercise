/**
 * @param {string} equation
 * @return {string}
 */
 var solveEquation = function(equation) {
    let q = equation.split('=');
    let symbol = [[],[]];
    for(let j = 0; j < 2; j++){
        for(let i = 0; i < q[j].length; i++){
            if(q[j][i] == '+' || q[j][i] == '-'){
                symbol[j].push(q[j][i]);
            }
        }
        q[j] = q[j].split(/\+|\-/g);
        if(q[j].length > symbol[j].length) symbol[j].unshift('+');  
    }
    const cal = (a,b,symbol,flag) => {
        a = parseInt(a) || 0;
        b = parseInt(b) || 0;
        switch(symbol){
            case '+':
                return flag ? a + b : a - b;
            case '-':
                return flag ? a - b : a + b;
        }
    };
    let x,n = 0;
    for(let i = 0; i < q.length; i++){
        for(let j = 0; j < q[i].length; j++){
            if(q[i][j].includes('x')){
                x = cal(x || 0,parseInt(q[i][j]) || (q[i][j][0] == '0' ? 0 : 1),symbol[i][j],i == 0);
            }else{
                n = cal(n,q[i][j],symbol[i][j],i == 1);
            }
        }
    }
    if(x == 0 && n == 0) return "Infinite solutions";
    if(!x) return "No solution";
    return "x=" + n / x;
};